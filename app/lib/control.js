var Alloy=require('alloy');

exports.getMainView=function(){
	return Alloy.createController('mainview');;
};

exports.getMenuView=function(){
	return Alloy.createController('menuview');	
};

exports.getMenuButton=function(args){
	var v=Ti.UI.createView({
		height: args.h,
		width: args.w,
		backgroundColor: '#00838F'
	});
	
	var b=Ti.UI.createView({
		height: "20dp",
		width: "20dp",
		backgroundImage: "/ic_menu.png"
	});
	
	v.add(b);
	
	return v;
};

//Get the Configuration Controller
exports.getConfigView=function(){
    return Alloy.createController('config');
};

//Get the Board of Directors Controller
exports.getBodView=function(){
    return Alloy.createController('bod');
};

//Get the Members Controller
exports.getMemberListView=function(){
    return Alloy.createController('members');
};

//Get the Members Controller
