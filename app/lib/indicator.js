function createIndicatorWindow(args) {
	var width = 200,
	    height = 50;

	var args = args || {};
	var top = args.top || 140;
	var text = args.text || 'Loading ...';

	var win = Titanium.UI.createWindow({
		height : height,
		width : width,
		top : top,
		borderRadius : 10,
		touchEnabled : false,
		backgroundColor : '#000',
		opacity : 0.6
	});

	var view = Ti.UI.createView({
		width : Ti.UI.SIZE,
		height : Ti.UI.FILL,
		center : {
			x : (width / 2),
			y : (height / 2)
		},
		layout : 'horizontal'
	});

	function osIndicatorStyle() {
		style = Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN;

		if ('iPhone OS' !== Ti.Platform.name) {
			style = Ti.UI.ActivityIndicatorStyle.DARK;
		}

		return style;
	}

	var activityIndicator = Ti.UI.createActivityIndicator({
		style : osIndicatorStyle(),
		left : 0,
		height : Ti.UI.FILL,
		width : 30
	});

	var progressBar = Ti.UI.createProgressBar({
		height : Ti.UI.FILL,
		left : 0,
		width : 200,
		min : 0,
		max : 100,
		value : 0,
		color : 'white',
		message : 'Downloading 0% of 100%S',
		font : {
			fontSize : 14,
			fontWeight : 'bold'
		},
		style : Titanium.UI.iPhone.ProgressBarStyle.PLAIN,
	});

	var label = Titanium.UI.createLabel({
		left : 10,
		width : Ti.UI.FILL,
		height : Ti.UI.FILL,
		text : text,
		color : '#fff',
		font : {
			fontFamily : 'Helvetica Neue',
			fontSize : 16,
			fontWeight : 'bold'
		}
	});

	function openIndicator() {
		view.add(activityIndicator);
		view.add(label);
		win.add(view);
		win.open();
		activityIndicator.show();
	}


	win.openIndicator = openIndicator;

	function closeIndicator() {
		activityIndicator.hide();
		win.close();
	}


	win.closeIndicator = closeIndicator;

	function showProgressBar() {
		view.add(progressBar);
		win.add(view);
		win.open();
	}


	win.showProgressBar = showProgressBar;

	function hideProgressBar() {
		win.close();
	}


	win.hideProgressBar = hideProgressBar;

	function updateProgress(args) {
		progressBar.message = 'Downloading ' + args.value + '% of 100%';
		progressBar.value = args.value;
	}


	win.updateProgress = updateProgress;

	return win;
}

// Public interface
exports.createIndicatorWindow = createIndicatorWindow;
