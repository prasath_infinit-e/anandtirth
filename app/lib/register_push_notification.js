var Alloy = require('alloy');
var constants = require('constants');
var login_util = require('login_util');

exports.register = function() {

	// If we already have the device token, then no point registering for it again.
	var deviceToken = Ti.App.Properties.getString(constants.APNS_DEVICE_TOKEN, '');
	// Check if the device is running iOS 8 or later
	if (Ti.Platform.name == "iPhone OS" && parseInt(Ti.Platform.version.split(".")[0]) >= 8) {

		// Wait for user settings to be registered before registering for push notifications
		Ti.App.iOS.addEventListener('usernotificationsettings', function registerForPush() {

			// Remove event listener once registered for push notifications
			Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);
			Ti.Network.registerForPushNotifications({
				success : deviceTokenSuccess,
				error : function(e) {
					Ti.API.info("OH NO FAILED");
				},
				callback : receivePush
			});
		});

		// Register notification types to use
		Ti.App.iOS.registerUserNotificationSettings({
			types : [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
		});
	}
	// For iOS 7 and earlier
	else {
		Ti.API.info("ios5");
		Ti.Network.registerForPushNotifications({
			// Specifies which notifications to receive
			types : [Ti.Network.NOTIFICATION_TYPE_BADGE, Ti.Network.NOTIFICATION_TYPE_ALERT, Ti.Network.NOTIFICATION_TYPE_SOUND],
			success : deviceTokenSuccess,
			error : function(e) {
				Ti.API.info("OH NO FAILED" + e.error);
			},
			callback : receivePush
		});
	}
};

// Process incoming push notifications
function receivePush(e) {
	Ti.API.info("gcm:works");
	// First check if the user is logged in or not. If it's not, then redirect him to login screen.
	/*
	 if (!login_util.isUserLoggedIn()) {
	 // User is not logged in - redirect to login screen
	 Alloy.createController('login_form').getView().open();
	 } else {
	 // Reset the app badge.
	 Titanium.UI.iPhone.appBadge = 0;
	 Alloy.createController('bday_anniversary_tab').getView().open();
	 }
	 */
}

// Save the device token for subsequent API calls
function deviceTokenSuccess(e) {
	var deviceToken = e.deviceToken;
	Ti.API.info("gcmDeviceToken :" + deviceToken);
	var existingDeviceToken = Ti.App.Properties.getString(constants.APNS_DEVICE_TOKEN, '');

	if (deviceToken == existingDeviceToken) {
		Ti.API.info("User already registered for notifications - not registering again");
		return;
	} else {
		if (existingDeviceToken == '') {
			Ti.API.info("Registering for PUSH notifications for the first time");
		} else {
			Ti.API.info("Updating PUSH notifications with updated deviceToken");
		}
	}

	var notificationModel = Alloy.createModel('notification');
	// Get the logged  in users name.
	var mobile = Ti.App.Properties.getString('mobile');
	var firstname = Ti.App.Properties.getString('firstname');
	var surname = Ti.App.Properties.getString('surnmame');
	var city = Ti.App.Properties.getString('city');
	notificationModel.fetch({
		urlparams : {
			"loginname" : mobile,
			"deviceid" : deviceToken,
			"mobileid" : Titanium.Platform.id,
			"devicetype" : 'ios',
			"firstname" : firstname,
			"surname" : surname,
			"city" : city
		},
		success : function() {
			var response = notificationModel.toJSON();
			Ti.API.info("Successfully registered for APNs");
			// At this point we have successfully registered for device
			Ti.App.Properties.setString(constants.APNS_DEVICE_TOKEN, deviceToken);
		},
		error : function() {
			alert('Failed to register for push notifications! ');
		}
	});
}

function deviceTokenError() {
	alert('Failed to register for push notifications! ' + e.error);
}