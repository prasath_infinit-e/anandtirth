var Alloy=require('alloy');

var constants = require('constants');

exports.isUserLoggedIn=function(){
	var loggedInUserId = Ti.App.Properties.getString('mobile','');
	if (loggedInUserId == '') {
	  return false;
	} else {
	  return true;	
	}
};

exports.loginUser=function(loginResponse, password){
	Ti.App.Properties.setString(constants.LOGGED_IN_MEMBER_INFO, JSON.stringify(loginResponse.member));	
	Ti.App.Properties.setString(constants.LOGGED_IN_USER_ID, loginResponse.member.id);
	Ti.App.Properties.setString(constants.LOGGED_IN_USER_PWD, password);
	Ti.App.Properties.setString(constants.LOGGED_IN_MEMBER_SPOUSE_INFO, JSON.stringify(loginResponse.spouse));
	Ti.App.Properties.setString(constants.LOGGED_IN_MEMBER_CHILD_INFO, JSON.stringify(loginResponse.children));
	Ti.App.Properties.setString(constants.LOGGED_IN_MEMBER_TYPE, loginResponse.memberType);	
	Ti.App.Properties.setString(constants.LOG_IN_RESPONSE, JSON.stringify(loginResponse));
};

exports.logoutUser=function() {
	Ti.App.Properties.removeProperty(constants.LOGGED_IN_USER_ID);
	Ti.App.Properties.removeProperty(constants.APNS_DEVICE_TOKEN);
	Ti.App.Properties.removeProperty(constants.LOGGED_IN_USER_PWD);
	Ti.App.Properties.removeProperty(constants.LOGGED_IN_MEMBER_INFO);
	Ti.App.Properties.removeProperty(constants.LOGGED_IN_MEMBER_SPOUSE_INFO);
	Ti.App.Properties.removeProperty(constants.LOGGED_IN_MEMBER_CHILD_INFO);
	Ti.App.Properties.removeProperty(constants.LOGGED_IN_MEMBER_TYPE);
};
