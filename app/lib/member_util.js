var Alloy = require('alloy');

var constants = require('constants');

var saveToContacts = function(memberName, phoneNumber) {
	Ti.Contacts.createPerson({
		firstName : memberName,
		phone : {
			mobile : [phoneNumber]
		}
	});
	var save_dialog = Ti.UI.createAlertDialog({
		message : 'Contact ' + memberName  + ' has been saved successfully',
		ok : 'OK',
		title : ''
	});
	save_dialog.show();
};

var createCallSmsWhatsappDialog = function(memberName, mobileNumber) {
	var b_dialog = Titanium.UI.createOptionDialog({
		options : ['Call', 'SMS', 'Save', 'Whatsapp', 'Cancel'],
		cancel : 4,
		title : 'Mobile:' + memberName
	});
	b_dialog.show();
	b_dialog.addEventListener('click', function(e) {
		switch(e.index) {
		case 0:
			Titanium.Platform.openURL('tel:' + mobileNumber);
			break;
		case 1:
			Titanium.Platform.openURL('sms:' + mobileNumber);
			break;
		case 2:
			if (Ti.Contacts.hasContactsPermissions()) {
				Ti.API.info('has cont permission');
				saveToContacts(memberName, mobileNumber);
			} else {
				Ti.API.info('has cont permission ELSE');
				
				Ti.Contacts.requestContactsPermissions(function(e) {
					
					Ti.API.info( e );
					
					if (e.success) {
						Ti.API.info('e.success');
						try{
						saveToContacts(memberName, mobileNumber);
						}catch(err){
							
						}
					} else {
						Ti.API.info('e.success else');
						var error_dialog = Ti.UI.createAlertDialog({
							message : 'You dont have permissions to add to Contacts.Turn on in Privacy Settings',
							ok : 'OK',
							title : 'no permission'
						});
						error_dialog.show();
					}
				});
			}
			break;
		case 3:
			try {
				Ti.Platform.openURL('whatsapp://app');
			} catch(e) {
				//alert(e);
				var whatsapp_alert = Ti.UI.createAlertDialog({
					message : 'There is no Whats app application installed',
					ok : 'OK'
				});
				whatsapp_alert.show();
			}
			break;
		}
	});
	return b_dialog;
};

module.exports = {
  saveToContacts: saveToContacts,
  createCallSmsWhatsappDialog: createCallSmsWhatsappDialog
};