// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var constants = require('constants');
var async = require('queue');
var db = Ti.Database.open('clubapp');
var files_arr = [];
var count = 0;
var offlineDetails = Alloy.createModel('offline');
var loader = require('indicator');
var progressBar;
var downloadSizeLength = 0;

function refresh(screen, callback) {
	Ti.API.info(screen);
	progressBar = loader.createIndicatorWindow({
		top : screen == 'home' ? 250 : 350,
		text : 'downloading....'
	});

	var loadingIndicator = loader.createIndicatorWindow({
		top : screen == 'home' ? 250 : 350,
		text : 'loading....'
	});
	loadingIndicator.openIndicator();
	offlineDetails.fetch({
		success : function() {
			loadingIndicator.closeIndicator();
			progressBar.showProgressBar();

			var response = offlineDetails.toJSON();
			downloadSizeLength = response.membersTable.length + response.spouseTable.length + response.childrenTable.length + response.advertisementTable.length;
			var myQueue = async.queue(process, 3);
			Ti.API.info("offline Response : " + JSON.stringify(response));
			storeMembers(response.membersTable);
			storeSpouse(response.spouseTable);
			storeChildren(response.childrenTable);
			storeBod(response.bodTable);
			storeAdvertisement(response.advertisementTable);
			myQueue.push(files_arr, function(_err) {
				_err && Ti.API.error("error processing queued model " + JSON.stringify(_err));

			});
			myQueue.drain = function() {
				Ti.API.info('all items have been processed');
				progressBar.hideProgressBar();
				callback();
			};
		},
		error : function() {
			Ti.API.info("No Network Connection !!!");
			loadingIndicator.closeIndicator();
			alert("Check Internet Connection !!!");
		}
	});
}

function storeMembers(members) {
	db.execute('DROP TABLE if exists members');
	db.execute('CREATE TABLE if not exists members (id INTEGER, name text, dob text, business text, mobile text, email text, facebook text, twitter text, bloodgroup text, bbm_pin text, dom text, res_add1 text, res_add2 text, res_add3 text, res_city text, res_zip text, res_phone text, off_add1 text, off_add2 text, off_add3 text, off_city text, off_zip text, off_phone text, photo text, website text, profcategory text, hobbies text, business_det text, fathersname text, mem_vat text, mem_tin text, logo text, extra_column1 text, extra_column2 text, extra_column3 text);');

	for (var i in members) {
		//Ti.API.info("Member Name : " + members[i].name);
		var memberId = members[i].id;
		var memberName = members[i].name;
		var memberDob = members[i].dob;
		var memberBusiness = members[i].business;
		var memberMobile = members[i].mobile;
		var memberEmail = members[i].email;
		var memberFacebook = members[i].facebook;
		var memberTwitter = members[i].twitter;
		var memberBloodGroup = members[i].bloodgroup;
		var memberBbmPin = members[i].bbmpin;
		var memberDom = members[i].dom;
		var memberResAdd1 = members[i].res_add1;
		var memberResAdd2 = members[i].res_add2;
		var memberResAdd3 = members[i].res_add3;
		var memberResCity = members[i].res_city;
		var memberResZip = members[i].res_zip;
		var memberResPhone = members[i].res_phone;
		var memberOffAdd1 = members[i].off_add1;
		var memberOffAdd2 = members[i].off_add2;
		var memberOffAdd3 = members[i].off_add3;
		var memberOffCity = members[i].off_city;
		var memberOffZip = members[i].off_zip;
		var memberOffPhone = members[i].off_phone;
		var memberPhoto = members[i].photo;
		var memberWebsite = members[i].website;
		var memberProfessionalCategory = members[i].profcategory;
		var memberHobbies = members[i].hobbies;
		var memberBusinessDetails = members[i].business_det;
		var memberFathersName = members[i].fathersname;
		var memberMemVat = members[i].mem_vat;
		var memberMemTin = members[i].mem_tin;
		var memberLogo = members[i].logo;
		var memberExtraColumn1 = members[i].extra_column1;
		var memberExtraColumn2 = members[i].extra_column2;
		var memberExtraColumn3 = members[i].extra_column3;

		db.execute('insert into members (id, name, dob, business, mobile, email, facebook, twitter, bloodgroup, bbm_pin, dom, res_add1, res_add2, res_add3 , res_city, res_zip, res_phone, off_add1, off_add2, off_add3, off_city, off_zip, off_phone, photo, website, profcategory, hobbies, business_det, fathersname, mem_vat, mem_tin, logo, extra_column1, extra_column2, extra_column3) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);', memberId, memberName, memberDob, memberBusiness, memberMobile, memberEmail, memberFacebook, memberTwitter, memberBloodGroup, memberBbmPin, memberDom, memberResAdd1, memberResAdd2, memberResAdd3, memberResCity, memberResZip, memberResPhone, memberOffAdd1, memberOffAdd2, memberOffAdd3, memberOffCity, memberOffZip, memberOffPhone, memberPhoto, memberWebsite, memberProfessionalCategory, memberHobbies, memberBusinessDetails, memberFathersName, memberMemVat, memberMemTin, memberLogo, memberExtraColumn1, memberExtraColumn2, memberExtraColumn3);
		storeImages("member", memberPhoto);
	}

}

/*** STORE SPOUSE DETAILS ***/

function storeSpouse(spouse) {
	db.execute('DROP TABLE if exists spouse');
	db.execute('CREATE TABLE if not exists spouse (id INTEGER, mid text, name text, dob text, business text, mobile text, email text, facebook text, twitter text, bloodgroup text, bbm_pin text, off_add1 text, off_add2 text, off_add3 text, off_city text, off_zip text, off_phone text, photo text, hobbies text);');

	for (var i in spouse) {
		//Ti.API.info("Member Name : " + members[i].name);
		var spouseId = spouse[i].id;
		var spouseMemberId = spouse[i].mid;
		var spouseName = spouse[i].name;
		var spouseDob = spouse[i].dob;
		var spouseBusiness = spouse[i].business;
		var spouseMobile = spouse[i].mobile;
		var spouseEmail = spouse[i].email;
		var spouseFacebook = spouse[i].facebook;
		var spouseTwitter = spouse[i].twitter;
		var spouseBloodGroup = spouse[i].bloodgroup;
		var spouseBbmPin = spouse[i].bbmpin;
		var spouseOffAdd1 = spouse[i].off_add1;
		var spouseOffAdd2 = spouse[i].off_add2;
		var spouseOffAdd3 = spouse[i].off_add3;
		var spouseOffCity = spouse[i].off_city;
		var spouseOffZip = spouse[i].off_zip;
		var spouseOffPhone = spouse[i].off_phone;
		var spousePhoto = spouse[i].photo;
		var spouseHobbies = spouse[i].hobbies;

		db.execute('insert into spouse (id, mid, name, dob, business, mobile, email, facebook, twitter, bloodgroup, bbm_pin, off_add1, off_add2, off_add3, off_city, off_zip, off_phone, photo, hobbies) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);', spouseId, spouseMemberId, spouseName, spouseDob, spouseBusiness, spouseMobile, spouseEmail, spouseFacebook, spouseTwitter, spouseBloodGroup, spouseBbmPin, spouseOffAdd1, spouseOffAdd2, spouseOffAdd3, spouseOffCity, spouseOffZip, spouseOffPhone, spousePhoto, spouseHobbies);
		storeImages("spouse", spousePhoto);
	}
}

/**** STORE CHILDRENS ****/

function storeChildren(childrens) {
	db.execute('DROP TABLE if exists childrens');
	db.execute('CREATE TABLE if not exists childrens (id INTEGER, mid text, name text, dob text, relation text, marital text, photo text);');

	for (var i in childrens) {
		//Ti.API.info("Member Name : " + members[i].name);
		var childId = childrens[i].id;
		var childMemberId = childrens[i].mid;
		var childName = childrens[i].name;
		var childDob = childrens[i].dob;
		var childRelation = childrens[i].relation;
		var childMarital = childrens[i].marital;
		var childPhoto = childrens[i].photo;

		db.execute('insert into childrens (id, mid, name, dob, relation, marital, photo) values (?,?,?,?,?,?,?);', childId, childMemberId, childName, childDob, childRelation, childMarital, childPhoto);
		storeImages("child", childPhoto);
	}
}

/*** STORE BOD ***/

function storeBod(bod) {
	db.execute('DROP TABLE if exists bod');
	db.execute('CREATE TABLE if not exists bod (id INTEGER, membername text, mid text, post text, mem_type text);');

	for (var i in bod) {
		var bodId = bod[i].id;
		var bodName = bod[i].membername;
		var bodMid = bod[i].mid;
		var bodPost = bod[i].post;
		var bodMemType = bod[i].mem_type;
		db.execute('insert into bod (id, membername, mid, post, mem_type) values (?,?,?,?,?);', bodId, bodName, bodMid, bodPost, bodMemType);
	}
}

/*** STORE ADVERTISEMENT ***/

function storeAdvertisement(ad) {
	db.execute('DROP TABLE if exists advertisement');
	db.execute('CREATE TABLE if not exists advertisement (id INTEGER, name text, ad_screen text, image text, linkout text, mobileno text);');

	for (var i in ad) {
		var adId = ad[i].id;
		var adName = ad[i].name;
		var adScreen = ad[i].ad_screen;
		var adImage = ad[i].image;
		var adLink = ad[i].linkout;
		var adMobile = ad[i].mobileno;
		db.execute('insert into advertisement (id, name, ad_screen, image, linkout, mobileno) values (?,?,?,?,?,?);', adId, adName, adScreen, adImage, adLink, adMobile);
		storeImages("ad", adImage);
	}
}

/*** FETCH ALL MEMBER DETAILS ****/

function fetchAllMembersList() {

	var memberInfo = [];

	try {
		var memberRow = db.execute('select * from members order by name');
		while (memberRow.isValidRow()) {
			var childInfo = [];
			var childRow = db.execute('select * from childrens where mid=' + memberRow.fieldByName('id'));
			while (childRow.isValidRow()) {
				childInfo.push({
					id : childRow.fieldByName('id'),
					mid : childRow.fieldByName('mid'),
					name : childRow.fieldByName('name'),
					dob : childRow.fieldByName('dob'),
					relation : childRow.fieldByName('relation'),
					marital : childRow.fieldByName('marital'),
					photo : childRow.fieldByName('photo')
				});
				childRow.next();
			}
			var spouseRow = db.execute('select * from spouse where mid=' + memberRow.fieldByName('id'));
			var spouseInfo = {
				name : spouseRow.fieldByName('name'),
				id : spouseRow.fieldByName('id'),
				mid : spouseRow.fieldByName('mid'),
				mem_type : 'spouse',
				dob : spouseRow.fieldByName('dob'),
				dom : memberRow.fieldByName('dom'),
				business : spouseRow.fieldByName('business'),
				mobile : spouseRow.fieldByName('mobile'),
				email : spouseRow.fieldByName('email'),
				facebook : spouseRow.fieldByName('facebook'),
				twitter : spouseRow.fieldByName('twitter'),
				bloodgroup : spouseRow.fieldByName('bloodgroup'),
				bbm_pin : spouseRow.fieldByName('bbm_pin'),
				res_add1 : memberRow.fieldByName('res_add1'),
				res_add2 : memberRow.fieldByName('res_add2'),
				res_add3 : memberRow.fieldByName('res_add3'),
				res_city : memberRow.fieldByName('res_city'),
				res_zip : memberRow.fieldByName('res_zip'),
				res_phone : memberRow.fieldByName('res_phone'),
				off_add1 : spouseRow.fieldByName('off_add1'),
				off_add2 : spouseRow.fieldByName('off_add2'),
				off_add3 : spouseRow.fieldByName('off_add3'),
				off_city : spouseRow.fieldByName('off_city'),
				off_zip : spouseRow.fieldByName('off_zip'),
				off_phone : spouseRow.fieldByName('off_phone'),
				photo : spouseRow.fieldByName('photo'),
				hobbies : spouseRow.fieldByName('hobbies')
			};
			memberInfo.push({
				id : memberRow.fieldByName('id'),
				name : memberRow.fieldByName('name'),
				mem_type : 'member',
				dob : memberRow.fieldByName('dob'),
				business : memberRow.fieldByName('business'),
				mobile : memberRow.fieldByName('mobile'),
				email : memberRow.fieldByName('email'),
				facebook : memberRow.fieldByName('facebook'),
				twitter : memberRow.fieldByName('twitter'),
				bloodgroup : memberRow.fieldByName('bloodgroup'),
				bbm_pin : memberRow.fieldByName('bbm_pin'),
				dom : memberRow.fieldByName('dom'),
				res_add1 : memberRow.fieldByName('res_add1'),
				res_add2 : memberRow.fieldByName('res_add2'),
				res_add3 : memberRow.fieldByName('res_add3'),
				res_city : memberRow.fieldByName('res_city'),
				res_zip : memberRow.fieldByName('res_zip'),
				res_phone : memberRow.fieldByName('res_phone'),
				off_add1 : memberRow.fieldByName('off_add1'),
				off_add2 : memberRow.fieldByName('off_add2'),
				off_add3 : memberRow.fieldByName('off_add3'),
				off_city : memberRow.fieldByName('off_city'),
				off_zip : memberRow.fieldByName('off_zip'),
				off_phone : memberRow.fieldByName('off_phone'),
				photo : memberRow.fieldByName('photo'),
				website : memberRow.fieldByName('website'),
				profcategory : memberRow.fieldByName('profcategory'),
				hobbies : memberRow.fieldByName('hobbies'),
				business_det : memberRow.fieldByName('business_det'),
				fathersname : memberRow.fieldByName('fathersname'),
				mem_vat : memberRow.fieldByName('mem_vat'),
				mem_tin : memberRow.fieldByName('mem_tin'),
				logo : memberRow.fieldByName('logo'),
				extra_column1 : memberRow.fieldByName('extra_column1'),
				extra_column2 : memberRow.fieldByName('extra_column2'),
				extra_column3 : memberRow.fieldByName('extra_column3'),
				FaceBook : memberRow.fieldByName('faceBook'),
				spouse : spouseInfo,
				children : childInfo
			});
			memberRow.next();
		}
		memberRow.close();
		Ti.API.info("AllMembersJson : " + JSON.stringify(memberInfo));
	} catch(SQLException) {
		Ti.API.info(SQLException);
	}
	return JSON.stringify(memberInfo);
}

/**** FETCH BOD ***/
function fetchBod() {
	var bodInfo = [];
	try {
		var bodRow = db.execute('select * from bod');
		while (bodRow.isValidRow()) {
			var memberRow = db.execute('select * from members where id=' + bodRow.fieldByName('mid'));
			bodInfo.push({
				id : memberRow.fieldByName('id'),
				membername : memberRow.fieldByName('name'),
				mid : memberRow.fieldByName('id'),
				post : bodRow.fieldByName('post'),
				mem_type : bodRow.fieldByName('mem_type'),
				name : memberRow.fieldByName('name'),
				dob : memberRow.fieldByName('dob'),
				business : memberRow.fieldByName('business'),
				mobile : memberRow.fieldByName('mobile'),
				email : memberRow.fieldByName('email'),
				facebook : memberRow.fieldByName('facebook'),
				twitter : memberRow.fieldByName('twitter'),
				bloodgroup : memberRow.fieldByName('bloodgroup'),
				bbm_pin : memberRow.fieldByName('bbm_pin'),
				dom : memberRow.fieldByName('dom'),
				res_add1 : memberRow.fieldByName('res_add1'),
				res_add2 : memberRow.fieldByName('res_add2'),
				res_add3 : memberRow.fieldByName('res_add3'),
				res_city : memberRow.fieldByName('res_city'),
				res_zip : memberRow.fieldByName('res_zip'),
				res_phone : memberRow.fieldByName('res_phone'),
				off_add1 : memberRow.fieldByName('off_add1'),
				off_add2 : memberRow.fieldByName('off_add2'),
				off_add3 : memberRow.fieldByName('off_add3'),
				off_city : memberRow.fieldByName('off_city'),
				off_zip : memberRow.fieldByName('off_zip'),
				off_phone : memberRow.fieldByName('off_phone'),
				photo : memberRow.fieldByName('photo'),
				website : memberRow.fieldByName('website'),
				profcategory : memberRow.fieldByName('profcategory'),
				hobbies : memberRow.fieldByName('hobbies'),
				business_det : memberRow.fieldByName('business_det'),
				fathersname : memberRow.fieldByName('fathersname'),
				mem_vat : memberRow.fieldByName('mem_vat'),
				mem_tin : memberRow.fieldByName('mem_tin'),
				logo : memberRow.fieldByName('logo'),
				extra_column1 : memberRow.fieldByName('extra_column1'),
				extra_column2 : memberRow.fieldByName('extra_column2'),
				extra_column3 : memberRow.fieldByName('extra_column3')

			});
			bodRow.next();
		}
	} catch(SQLException) {
		Ti.API.info(SQLException);
	}

	return JSON.stringify(bodInfo);
}

/*** FETCH ADVERTISEMENT ***/
function fetchAdvertisement() {
	var adInfo = '';
	var bannerAdInfo = [];
	var fullAdInfo = [];
	try {
		var adRow = db.execute('select * from advertisement');
		while (adRow.isValidRow()) {
			var ad = {
				id : adRow.fieldByName('id'),
				name : adRow.fieldByName('name'),
				ad_screen : adRow.fieldByName('ad_screen'),
				image : adRow.fieldByName('image'),
				linkout : adRow.fieldByName('linkout'),
				mobileno : adRow.fieldByName('mobileno'),
			};
			if (adRow.fieldByName('ad_screen') == 'Banner') {
				bannerAdInfo.push(ad);
			} else {
				fullAdInfo.push(ad);
			}
			adRow.next();
		}
		adInfo = {
			ad_details : bannerAdInfo,
			ad_fullpage : fullAdInfo
		};
	} catch(SQLException) {
		Ti.API.info(SQLException);
	}
	Ti.API.info(JSON.stringify(adInfo));
	return JSON.stringify(adInfo);
}

function process(_url, _processCallback) {

	// download the file
	get_file(_url, function(_resp) {
		if (!_resp.error) {
			Ti.API.debug('file successfully downloaded ' + _url);
		} else {
			Ti.API.error('Error Downloading file ' + _url);
			Ti.API.error('Error Message ' + _resp.error);
		}
		_processCallback(_resp.error);
	}, null);
};

function urlToObject(url) {
	var returnObj;
	url = url.replace('URLSCHEMENAME://?', '');
	var params = url.split('&');
	var filepath = params[1].split('=');
	returnObj = filepath[1];
	return returnObj;
}

var get_file = function(url, callback) {
	count++;
	progressBar.updateProgress({
		value : parseInt(count * 100 / downloadSizeLength)
	});
	if (Ti.Network.online) {
		var xhr = Ti.Network.createHTTPClient({
			onload : function(e) {
				console.log('downloaded ' + url);
				var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, urlToObject(url));
				f.write(this.responseData);
				Ti.API.info(f);
				callback({
					url : url,
					data : this.responseData
				});
			},
			onerror : function(e) {
				console.log('error ' + url);

				callback({
					url : url,
					error : e.error
				});
			},
			timeout : 5000
		});
		xhr.open("GET", url);
		xhr.send();
	}
};

try {
	function storeImages(type, filename) {
		var url;
		switch(type) {
		case "member":
		case "spouse":
		case "child":
			url = constants.BACKEND_URL + 'photo?photo_type=members&photo_name=' + filename;
			break;
		case "ad":
			url = constants.BACKEND_URL + 'photo?photo_type=ads&photo_name=' + filename;
			break;
		}
		files_arr.push(url);
	}

} catch(e) {
	//Ti.API.info(e);
}

exports.refresh = refresh;
exports.fetchAllMembersList = fetchAllMembersList;
exports.fetchBod = fetchBod;
exports.fetchAdvertisement = fetchAdvertisement;
