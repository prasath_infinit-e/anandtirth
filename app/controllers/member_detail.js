var member_util = require('member_util');
var constants = require('constants');

var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
Ti.API.info(data);
//Ti.API.info(constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[0].image);
Ti.API.info(data.ad_details.length);

if (data.ad_details.length > 0) {
	var currentAd = 0;
	//$.adImgView.setImage(constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[0].image);
	setInterval(function() {

		if (currentAd >= data.ad_details.length - 1) {
			currentAd = 0;
		} else {
			currentAd += 1;
		}
		$.adImgView.setImage(constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[currentAd].image);
	}, 3500);

}
function adclick(e) {
	var ad_dialog = Titanium.UI.createOptionDialog({
		options : ['Call', 'Open in browser', 'Cancel'],
		cancel : 3,
	});
	ad_dialog.show();

	ad_dialog.addEventListener('click', function(e) {

		switch(e.index) {

		case 0:
			Titanium.Platform.openURL('tel:' + data.ad_details[currentAd].mobileno);
			break;
		case 1:
			Ti.Platform.openURL(data.ad_details[currentAd].linkout);
			break;
		}
	});
}

var mainResponse;

(function constructor(args) {

	// use strict mode for this function scope
	'use strict';

	// model passed to the controller
	var id = args.id;
	var memberDetail = args.member_detail;
	$.activityIndicator.show();
	if (id) {
		memberDetail = Alloy.createModel('members', {
			id : id
		});
		memberDetail.fetch({
			success : function() {
				var response = memberDetail.toJSON();
				mainResponse = response;
				$.activityIndicator.hide();
				displayResponse(response.member, response.spouse.name, response.children, true);
			},
			error : function() {
				$.activityIndicator.hide();
				alert("Check internet connection");
			}
		});
	} else if (memberDetail) {
		mainResponse = memberDetail;
		// WE do not display children info while coming from search.
		displayResponse(mainResponse, mainResponse.spouse_name, null/* childrenInfo */, false);
		$.activityIndicator.hide();
	}
	// execute constructor with optional arguments passed to controller
})(arguments[0] || {});

function displayResponse(response, spouseName, children, navigateToSpouseInfo) {
	var validSections = [];
	validSections.push($.mainImage);
	validSections.push($.personalInfo);
	// THE URL should come from a constant. Now it's all over the place.
	$.memImage.setImage(constants.BACKEND_IMAGE_URL + "members/" + escape(response.photo));
	$.name_lbl.setText(response.name);
	$.mobile.setText(response.mobile);
	if (spouseName == null) {
		$.memberDetailTable.deleteRow($.row1);
	} else {
		$.name.setText(spouseName);
		// We do not have spouse info when coming from search, so we don't want to honor the
		// the click event if we don't have that info.
		if (navigateToSpouseInfo) {
			$.row1.addEventListener('click', function(e) {
				gotospouse();
			});
		}
	}
	if (spouseName == null) {
		$.memberDetailTable.deleteRow($.row2);
	} else {
		$.dom.setText(new Date(response.dom).toLocaleDateString('en-in'));
	}
	$.email.setText(response.email);
	$.dob.setText(new Date(response.dob).toLocaleDateString('en-in'));
	$.bloodgroup.setText(response.bloodgroup);
	$.profcategory.setText(response.business);
	if (response.res_add1) {
		validSections.push($.residenceAddress);
		$.res_add1.setText(response.res_add1);
		if (response.res_add2) {
			$.res_add2.setText(response.res_add2);
		}
		if (response.res_add3) {
			$.res_add3.setText(response.res_add3);
		}
		if (response.res_city) {
			$.res_city.setText(response.res_city);
		}
		if (response.res_zip) {
			$.res_zip.setText(response.res_zip);
		}
	}
	if (response.off_add1) {
		validSections.push($.officeAddress);
		$.off_add1.setText(response.off_add1);
		if (response.off_add2) {
			$.off_add2.setText(response.off_add2);
		}
		if (response.off_add3) {
			$.off_add3.setText(response.off_add3);
		}
		if (response.off_city) {
			$.off_city.setText(response.off_city);
		}
		if (response.off_zip) {
			$.off_zip.setText(response.off_zip);
		}
	}
	Ti.API.info("childrens", children);
	if (children == null) {
	} else {
		if (children.length <= 0) {
			$.memberDetailTable.deleteSection(3);
		} else {
			validSections.push($.childrenInfo);
			bindChildren(children);
		}
	}
	$.memberDetailTable.setData(validSections);
}

//---------------------------------Call, SMS, Save, Whatsapp----------------------------------//
$.row3.addEventListener('click', function(e) {
	member_util.createCallSmsWhatsappDialog($.name_lbl.text, $.mobile.text);
});

//-----------------------------Email------------------------------------//
$.row4.addEventListener('click', function(e) {
	var emailDialog = Titanium.UI.createEmailDialog();
	if (!emailDialog.isSupported()) {
		Ti.UI.createAlertDialog({
			title : 'Error',
			message : 'Email not available on this device.'
		}).show();
		return;
	}
	emailDialog.setSubject(' Gmail ');
	emailDialog.setToRecipients([$.email.text]);
	emailDialog.setMessageBody('Hi,\n');
	emailDialog.setHtml(false);
	// emailDialog.setBarColor('#336699');
	emailDialog.open();
	emailDialog.addEventListener('complete', function(e) {
		if (e.result == emailDialog.SENT) {
			var email_alert = Ti.UI.createAlertDialog({
				message : 'Email sent successfully',
				ok : 'OK',
				title : 'Email'
			});
			email_alert.show();
		} else {
			var err_alert = Ti.UI.createAlertDialog({
				message : 'Email is not sent',
				ok : 'OK',
				title : 'Email Error'
			});
			err_alert.show();
		}
	});
});

//--------------------------Children Info ------------------------------//

function bindChildren(childrens) {
	var IMG_BASE = constants.BACKEND_URL + "photo?photo_type=members&photo_name=";
	var defaultFontSize = Ti.Platform.name === 'android' ? 16 : 14;

	var tableData = [];

	for (var i in childrens) {
		var row = Ti.UI.createTableViewRow({
			className : 'forumEvent',
			selectedBackgroundColor : 'white',
			rowIndex : i,
			height : 70
		});

		var imageAvatar = Ti.UI.createImageView({
			image : IMG_BASE + childrens[i].photo,
			left : 10,
			top : 5,
			width : 50,
			height : 50
		});
		row.add(imageAvatar);

		var childName = Ti.UI.createLabel({
			color : 'black',
			font : {
				//fontFamily : 'Arial',
				fontSize : 14,
				//fontWeight : 'bold'
			},
			text : "Name :" + " " + childrens[i].name,
			left : 70,
			top : 2,
			width : Ti.UI.SIZE,
			height : Ti.UI.SIZE,
			id : "ChildName" + i
		});
		row.add(childName);

		/*var imageCalendar = Ti.UI.createImageView({
		 image : '/eventsButton.png',
		 left : 70,
		 bottom : 5,
		 width : 32,
		 height : 32
		 });
		 row.add(imageCalendar);*/

		var childDob = Ti.UI.createLabel({
			color : 'black',
			font : {
				//fontFamily : 'Arial',
				fontSize : 14,
				//fontWeight : 'bold'
			},
			text : "DOB :" + " " + new Date(childrens[i].dob).toLocaleDateString('en-in'),
			left : 70,
			bottom : 2,
			width : Ti.UI.SIZE,
			height : Ti.UI.SIZE,
			id : i
		});
		row.add(childDob);

		/*childDob.addEventListener('touchstart', function(e) {
		 Ti.API.info(i);
		 showDatePicker(e);
		 });*/

		tableData.push(row);
	}

	$.childTableView.setData(tableData);
}

/**
 * Event listener set via view to be called on when the user taps the home-icon (Android)
 */


function gotospouse() {
	Alloy.createController('spouse_detail', {
		response : mainResponse
	}).getView().open();
}

function close() {
	'use strict';
	// close the window, showing the master window behind it
	$.navWin.close();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}