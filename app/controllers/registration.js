var constants = require('constants');
var args = $.args;
var eventid = args.event;
var eventtype = args.type;
Ti.API.info("EventInfo :" + eventid);
/*var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);

if (data.ad_details.length > 0) {
	var currentAd = 0;
	setInterval(function() {

		if (currentAd >= data.ad_details.length - 1) {
			currentAd = 0;
		} else {
			currentAd += 1;
		}
		$.adImgView.setImage(Ti.App.Properties.getBool('dbIntialized') ? Ti.Filesystem.applicationDataDirectory + "/" + data.ad_details[currentAd].image : constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[currentAd].image);
	}, 3500);

}
function adclick(e) {
	var ad_dialog = Titanium.UI.createOptionDialog({
		options : ['Call', 'Website', 'Cancel'],
		cancel : 3,
	});
	ad_dialog.show();

	ad_dialog.addEventListener('click', function(e) {

		switch(e.index) {

		case 0:
			Titanium.Platform.openURL('tel:' + data.ad_details[currentAd].mobileno);
			break;
		case 1:
			Ti.Platform.openURL(data.ad_details[currentAd].linkout);
			break;
		}
	});
}*/

//display in dropdown

var collection = Alloy.Collections.registration;
collection.fetch({
	success : function() {

		var events = collection.toJSON();
		Ti.API.info("Registration Event Length : " + events.length);
		if (events.length > 0) {
			bindOptions(events);
		} else {

			alert("There is no events available");
		}
	},
	error : function() {
		alert('Please Check Internet Connection');
	}
});

function bindOptions(events) {
	var dialog_options = [];
	for (var i = 0; i < events.length; i++) {
		if (events[i].id == eventid) {
			$.txtEvent.value = events[i].name;
		}
		var event_name = events[i].name;
		dialog_options.push(event_name);
	}

	dialog_options.push('Cancel');

	$.dialog.options = dialog_options;
	$.dialog.cancel = dialog_options.length - 1;
	$.dialog.addEventListener('click', function(e) {
		if (e.index == dialog_options.length - 1) {
			$.dialog.hide();
		} else {
			eventid = events[e.index].id;
			eventtype = events[e.index].event_type;
			$.txtEvent.value = dialog_options[e.index];
		}
	});
}

Ti.API.info("EventInfo :" + eventid);

function createDialog(message) {
	var dialog = Ti.UI.createAlertDialog({
		buttonNames : ['OK'],
		message : message,
		title : 'Message'
	});
	dialog.show();
}

function validateField(field, message) {
	if (field.value == '') {
		createDialog(message);
		return false;
	}
	return true;
}

function submit() {
	$.activityIndicator.show();
	if (!validateField($.txtEvent, 'event name cannot be empty')) {
		return;
	}
	if (!validateField($.txtname, 'enter your name')) {
		return;
	}
	if (!validateField($.txtmobile, 'enter your mobile number')) {
		return;
	}
	if (!validateField($.txtResAdd1, 'enter your address line 1')) {
		return;
	}
	if (!validateField($.txtResAdd2, 'enter your address line 2')) {
		return;
	}
	if (!validateField($.txtResCity, 'enter your city')) {
		return;
	}
	if (!validateField($.txtphone, 'enter your landline number')) {
		return;
	}

	var Info = {
		event_id : eventid,
		event_type : eventtype,
		membername : $.txtname.value,
		mobile : $.txtmobile.value,
		email_id : $.txtEmail.value,
		address1 : $.txtResAdd1.value,
		address2 : $.txtResAdd2.value,
		addr_city : $.txtResCity.value,
		addr_zip : $.txtpincode.value,
		addr_phone : $.txtphone.value
	};

	var memberDetails = JSON.stringify(Info);

	Ti.API.info(memberDetails);

	var params = {
		member_details : memberDetails,
		success : function() {

			alert('Thank You, Registered Successfully ');
			$.activityIndicator.hide();
		},
		error : function() {
			$.activityIndicator.show();
			alert('Registration failed, check ur internet');
			$.activityIndicator.hide();
		}
	};
	transmit(params);
}

function transmit(params) {
	$.activityIndicator.show();
	var xhr = Titanium.Network.createHTTPClient();
	xhr.onload = function() {
		$.activityIndicator.show();
		alert("Thank you,Registration successful");

	};
	xhr.open("POST", constants.BACKEND_URL + "event_register/");
	$.activityIndicator.hide();
	xhr.onerror = function() {
		alert("Registration Failed,check your Internet");
	};

	xhr.setRequestHeader("enctype", "multipart/form-data");

	xhr.send(params);
}

function selectevent() {
	'use strict';
	// close the window, showing the master window behind it
	$.dialog.show();

}

/*
 $.dialog.options.addEventListener('click', function(e) {
 $.txtEvent.value = dialog_options.options[e.index];
 });
 */

function close() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}