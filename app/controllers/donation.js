var constants = require('constants');
var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
var currentAd = 0;

if (data.ad_details.length > 0) {

	setInterval(function() {

		if (currentAd >= data.ad_details.length - 1) {
			currentAd = 0;
		} else {
			currentAd += 1;
		}
		$.adImgView.setImage(Ti.App.Properties.getBool('dbIntialized') ? Ti.Filesystem.applicationDataDirectory + "/" + data.ad_details[currentAd].image : constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[currentAd].image);
	}, 3500);

}
function adclick(e) {
	var ad_dialog = Titanium.UI.createOptionDialog({
		options : ['Call', 'Website', 'Cancel'],
		cancel : 3,
	});
	ad_dialog.show();

	ad_dialog.addEventListener('click', function(e) {

		switch(e.index) {

		case 0:
			Titanium.Platform.openURL('tel:' + data.ad_details[currentAd].mobileno);
			break;
		case 1:
			Ti.Platform.openURL(data.ad_details[currentAd].linkout);
			break;
		}
	});
}

//display in dropdown

function createDialog(message) {
	var dialog = Ti.UI.createAlertDialog({
		buttonNames : ['OK'],
		message : message,
		title : 'Message'
	});
	dialog.show();
}

function validateField(field, message) {
	if (field.value == '') {
		createDialog(message);
		return false;
	}
	return true;
}

var opts = {
	options : ['', 'Education', 'Medical', 'Marriage', 'Others'],
};
var dialog = Ti.UI.createOptionDialog(opts);
$.txtPurpose.addEventListener('click', function(e) {
	dialog.show();
	dialog.addEventListener('click', function(e) {
		$.txtPurpose.value = opts.options[e.index];
	});
});

function donate() {
	$.activityIndicator.show();
	if (!validateField($.txtName, 'enter your name')) {
		return;
	}
	if (!validateField($.txtMobile, 'enter your mobile number')) {
		return;
	}
	if (!validateField($.txtResAdd1, 'enter your address line 1')) {
		return;
	}
	if (!validateField($.txtResAdd2, 'enter your address line 2')) {
		return;
	}
	if (!validateField($.txtZipcode, 'enter your pincode')) {
		return;
	}
	if (!validateField($.txtPurpose, 'enter the puropse of donate')) {
		return;
	}

	var memberInfo = {
		name : $.txtName.value,
		mobile : $.txtMobile.value,
		email : $.txtEmail.value,
		address1 : $.txtResAdd1.value,
		address2 : $.txtResAdd2.value,
		city : "",
		state : "",
		zipcode : $.txtZipcode.value,
		purpose : $.txtPurpose.value
	};

	var memberDetails = JSON.stringify(memberInfo);

	Ti.API.info(memberDetails);

	var params = {
		donation : memberDetails,
		success : function() {
			alert('Thank You, Daan Successfull ');
		},
		error : function() {
			alert('Daan failed, check your internet');
		}
	};
	transmit(params);
}

function transmit(params) {
	$.activityIndicator.show();
	var xhr = Titanium.Network.createHTTPClient();
	xhr.onload = function() {
		$.activityIndicator.show();
		alert("Thank you,Donation successful");
	};
	xhr.open("POST", constants.BACKEND_URL + "anandtrith_donation");
	$.activityIndicator.hide();
	xhr.onerror = function() {
		alert("Donation Failed, check your internet");
	};
	xhr.setRequestHeader("enctype", "multipart/form-data");

	xhr.send(params);
}

/*var collection = Alloy.Collections.donation;
 collection.fetch({
 success : function() {
 $.activityIndicator.show();
 var events = collection.toJSON();
 Ti.API.info("Registration Event Length : " + events.length);
 if (events.length > 0) {
 bindOptions(events);
 } else {
 $.activityIndicator.hide();
 alert("There is no events available");
 }
 },
 error : function() {
 alert('Please Check Internet Connection');
 }
 });

 function bindOptions(events) {
 var dialog_options = [];
 for (var i = 0; i < events.length; i++) {
 var event_name = events[i].purpose;
 dialog_options.push(event_name);
 }

 $.dialog.options = dialog_options;
 }*/

/*
 var c = Titanium.Network.createHTTPClient();
 c.onload = function()
 {
 var xml=this.responseXML.documentElement;
 doc=xml.getElementsByTagName("PLANT");
 for(var a=0;a<doc.length;a++)
 {
 data.push(Ti.UI.createPickerRow({title:doc.item(a).getElementsByTagName("COMMON").item(0).text}));
 }
 $.picker.add(data);
 };
 c.onerror = function(e)
 {
 Ti.API.info('XHR Error ' + e.error);
 };
 c.open('GET', 'http://www.codehouse.in/anandtirth/api/all_events_list');
 c.send();

 var profession_opts = {
 options : ['', 'Apparels & Garments', 'Automobile', 'Construction/Bldg.Material', 'Computers / Mobiles', 'Doctor', 'Electronics/Electrical', 'Home Appliances', 'Event Mgmt/Hospitality', 'Food/Beverages/Oil', 'Gems & Jewellery', 'Industrial Supplies', 'Legal/Tax/Consultant', 'Medical Products', 'Metals & Minerals', 'Mortgage & Finance', 'Others', 'Packaging', 'Paper / Printing', 'Plant & Machinery', 'Plastic & Plastic Products', 'Profession', 'Stock Broking/Chit funds', 'Textile/Yarn/Fabrics', 'Timber/Plywood/Hardware', 'Transport / Logistics'],
 };
 var dialog1 = Ti.UI.createOptionDialog(profession_opts);
 $.proftext.addEventListener('click', function(e) {
 dialog1.show();
 dialog1.addEventListener('click', function(e) {
 $.proftext.value = profession_opts.options[e.index];
 });
 });
 */

/*
 function transformEventInfo(model) {
 $.activityIndicator.hide();
 // Need to convert the model to a JSON object
 var transform = model.toJSON();
 Ti.API.info("EVENTSLENGTH " + transform.length);
 Ti.API.info("EVENTSLENGTH " + transform.name);
 var strEventName = replaceAll(" ", "_", transform.name);
 Ti.API.info("event Name: " + strEventName);
 transform.invitation = constants.BACKEND_IMAGE_URL + "events/" + transform.id + "_" + strEventName.toLowerCase() + "/" + transform.invitation;
 Ti.API.info(transform.invitation);
 transform.eventdate = transform.eventdate;
 return transform;

 }*/

function replaceAll(find, replace, str) {
	var re = new RegExp(find, 'g');
	str = str.replace(re, replace);
	return str;
}

function close() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}