var constants = require('constants');
var args = $.args;
var eventid = args.event;
var eventtype = args.type;
Ti.API.info("EventInfo :" + eventid);
/*var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
Ti.API.info(data);
Ti.API.info(data.ad_details.length);

if (data.ad_details.length > 0) {
	var currentAd = 0;
	setInterval(function() {

		if (currentAd >= data.ad_details.length - 1) {
			currentAd = 0;
		} else {
			currentAd += 1;
		}
		$.adImgView.setImage(Ti.App.Properties.getBool('dbIntialized') ? Ti.Filesystem.applicationDataDirectory + "/" + data.ad_details[currentAd].image : constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[currentAd].image);
	}, 3500);

}
function adclick(e) {
	var ad_dialog = Titanium.UI.createOptionDialog({
		options : ['Call', 'Website', 'Cancel'],
		cancel : 3,
	});
	ad_dialog.show();

	ad_dialog.addEventListener('click', function(e) {

		switch(e.index) {

		case 0:
			Titanium.Platform.openURL('tel:' + data.ad_details[currentAd].mobileno);
			break;
		case 1:
			Ti.Platform.openURL(data.ad_details[currentAd].linkout);
			break;
		}
	});
}*/

//song

var args = $.args;
var songId = args.song;
var audMin = args.min;
var songUrl = '';

switch(songId) {
case 0:
	songUrl = '/Billa.mp3';
	break;
case 1:
	songUrl = '/music1.mp3';
	break;
case 2:
	songUrl = '/music2.mp3';
	break;
}


var audioPlayer = Ti.Media.createAudioPlayer({
	url : songUrl,
	allowBackground : false
});
audioPlayer.addEventListener('progress',function(e) 
{
   var newValue = parseValue(e.progress, 1);
   
   label.text = Math.round((newValue/1000) % 60);

   // set the progress bar's progress value with current time played.. (milliseconds) 
   pb.value =  Math.round(e.progress/1000) ; 
});
audioPlayer.start();

var finalTime = audioPlayer.getDuration();
   Ti.API.info('final time: ' + finalTime );


var pb=Titanium.UI.createSlider
({
   top:100,
   width:250,
   height : "auto",
   min:0,
   max:1000,
   value:0,
   color:'#fff',
});
$.songwin.add(pb);
pb.show();

var label = Ti.UI.createLabel({
	top: 20,
	left : 250,
	top : 3,
	color : "black",
	
	text : ""
});
$.songwin.add(label);



function parseValue(value, value_to_round) {
    value /= value_to_round;
    value = Math.round(value);
    value *= value_to_round;
   return value;
};

var btnPlay = Ti.UI.createImageView({
	image : '/Play.png',
	bottom : 100,
	width : '80dp',
	height : '80dp',
	right : '35%'
});

var btnStop = Ti.UI.createImageView({
	image : '/Stop.png',
	bottom : 110,
	width : '40dp',
	height : '40dp',
	right : '10%'
});

var btnPause = Ti.UI.createImageView({
	image : '/Pause.png',
	width : '40dp',
	height : '40dp',
	right : '77%',
	bottom : 110
});

btnPlay.addEventListener('click', function() {
	//if(audioPlayer.pause){
	audioPlayer.start();
	//}
});

btnStop.addEventListener('click', function() {

	audioPlayer.stop();
});

btnPause.addEventListener('click', function() {

	audioPlayer.pause();
});
$.songwin.add(btnPlay);
$.songwin.add(btnStop);
$.songwin.add(btnPause);

$.songwin.addEventListener('close', windowClosed);
function windowClosed() {
	audioPlayer.stop();
}

function close() {
	audioPlayer.stop();
	Alloy.createController('meditation').getView().open();

}

function home() {
	audioPlayer.stop();
	'use strict';
	// close the window, showing the master window behind it

	Alloy.createController('index').getView().open();
}