var constants = require('constants');

//advertisemnet
var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
var currentAd = 0;

if (data.ad_details.length > 0){
//$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ 0 ].image );
setInterval(function(){ 
	
	
	if( currentAd >= data.ad_details.length-1 ){
		currentAd = 0;
	}else{
		currentAd+=1;
	}
	$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ currentAd ].image );
	 }, 3500);

}
function adclick(e){
	  var ad_dialog = Titanium.UI.createOptionDialog({
					options : ['Call','Website','Cancel'],
					cancel : 3,
				});
				ad_dialog.show();
				
				
				ad_dialog.addEventListener('click', function(e) {
					
					switch(e.index) {
					
					case 0:
						Titanium.Platform.openURL('tel:' + data.ad_details[ currentAd ].mobileno);
						break;
					case 1:
						Ti.Platform.openURL( data.ad_details[ currentAd ].linkout);
						break;
					}
				});
			}

//bodview
var moment = require('alloy/moment');
var controls=require('control');

var BoardOfDirectors = Alloy.Collections.bod;
BoardOfDirectors.fetch({
	success:function(){},
	error:function(){
		alert('Please Check Internet Connection');
	}
});

/**
 * self-executing function to organize otherwise inline constructor code
 * @param  {Object} args arguments passed to the controller
 */
(function constructor(args) {

	// use strict mode for this function scope
	'use strict';

	// use the refresh callback for the initial load
	refresh();

	// execute constructor with optional arguments passed to controller
})(arguments[0] || {});


/**
 * event listener added via view for the refreshControl (iOS) or button (Android)
 * @param  {Object} e Event, unless it was called from the constructor
 */
function refresh(e) {
	BoardOfDirectors.fetch();
}
$.activityIndicator.show();

function transformBodInfo(model) {
	$.activityIndicator.hide();
    // Need to convert the model to a JSON object
    var transform = model.toJSON();
    transform.photo = constants.BACKEND_IMAGE_URL+"members/" + transform.photo;    
    return transform;
}

/**
 * event listener set via view for when the user selects a ListView item
 * @param  {Object} e Event
 */
function select(e) {
	'use strict';
	// lookup the model based on the index.
	var model = BoardOfDirectors.at(e.itemIndex);
	// create the detail controller with the model and get its view
	Alloy.createController('bod_detail', {
		model: model
	}).getView().open();
}

function close() {
	'use strict';
	// close the window, showing the master window behind it
	$.navWin.close();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	 Alloy.createController('index').getView().open();
}

function cleanup() {
    $.destroy();
}