var opts = {
		options : ['', 'A+', 'A-', 'AB+', 'AB-', 'B+', 'B-', 'O+', 'O-'],
	};
	var dialog = Ti.UI.createOptionDialog(opts);
	$.textopt.addEventListener('click', function(e) {
		dialog.show();
		dialog.addEventListener('click', function(e) {
			$.textopt.value = opts.options[e.index];
		});
	});

var profession_opts = {
		options : ['', 'Apparels & Garments', 'Automobile', 'Construction/Bldg.Material', 'Computers / Mobiles', 'Doctor', 'Electronics/Electrical', 'Home Appliances', 'Event Mgmt/Hospitality', 'Food/Beverages/Oil', 'Gems & Jewellery', 'Industrial Supplies', 'Legal/Tax/Consultant', 'Medical Products', 'Metals & Minerals', 'Mortgage & Finance', 'Others', 'Packaging', 'Paper / Printing', 'Plant & Machinery', 'Plastic & Plastic Products', 'Profession', 'Stock Broking/Chit funds', 'Textile/Yarn/Fabrics', 'Timber/Plywood/Hardware', 'Transport / Logistics'],
	};
	var dialog1 = Ti.UI.createOptionDialog(profession_opts);
	$.proftext.addEventListener('click', function(e) {
		dialog1.show();
		dialog1.addEventListener('click', function(e) {
			$.proftext.value = profession_opts.options[e.index];
		});
	});
	
$.submit.addEventListener('click', function(e) {
	
		if ($.nametext.value == '' && $.phtext.value == '' && $.proftext.value == '' && $.textopt.value == '') {
			alert('Any one of the field is required');
		} else {
			$.activityIndicator.show();
			var searchModel = Alloy.createModel('searchresult');
			
searchModel.fetch({
	urlparams : {
		"name" : $.nametext.value,
		"blood" : $.textopt.value,
		"phone":$.phtext.value,
		"business":$.proftext.value
		
	},
	success : function() {
		$.activityIndicator.hide();
		var response = searchModel.toJSON();
		if(response.search_results.length > 0){
			Alloy.createController('searchresult', {
			response: response}).getView().open();
		}else{
			alert("No Result Found");
		}
		//Ti.API.info("search response" + response.search_results[0].id);
	},
	error : function() {
		$.activityIndicator.hide();
		alert("Check internet connection");
	}
});
		}
			
			});
			
			
function home() {
	'use strict';
	// close the window, showing the master window behind it
	 Alloy.createController('index').getView().open();
}
		
		

function close() {
	'use strict';
	// close the window, showing the master window behind it
	$.navWin.close();
}
//var args = arguments[0] || {};