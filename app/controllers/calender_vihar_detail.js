/**
 * self-executing function to organize otherwise inline constructor code
 * @param  {Object} args arguments passed to the controller
 */
var constants = require('constants');
//var rsvp_event_id;
//var eventvideo;
var event;
var eventtype;
/*var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
Ti.API.info(data);
Ti.API.info(data.ad_details.length);

if (data.ad_details.length > 0){
var currentAd = 0;
setInterval(function(){ 
	
	
	if( currentAd >= data.ad_details.length-1 ){
		currentAd = 0;
	}else{
		currentAd+=1;
	}
	$.adImgView.setImage(Ti.App.Properties.getBool('dbIntialized') ? Ti.Filesystem.applicationDataDirectory + "/" + data.ad_details[currentAd].image : constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[currentAd].image);
	 }, 3500);

}
function adclick(e){
	  var ad_dialog = Titanium.UI.createOptionDialog({
					options : ['Call','Website','Cancel'],
					cancel : 3,
				});
				ad_dialog.show();
				
				
				ad_dialog.addEventListener('click', function(e) {
					
					switch(e.index) {
					
					case 0:
						Titanium.Platform.openURL('tel:' + data.ad_details[ currentAd ].mobileno);
						break;
					case 1:
						Ti.Platform.openURL( data.ad_details[ currentAd ].linkout);
						break;
					}
				});
			}*/


//EventDetailview

(function constructor(args) {

	// use strict mode for this function scope
	'use strict';

	// model passed to the controller
	var model = args.model.toJSON();

	if (model) {
		// SHOW the image as well.
		//rsvp_event_id=model.id;
		event = model;
		//$.eventImage.setImage(constants.BACKEND_IMAGE_URL+"events/"  + model.invitation);

		$.eventImage.setImage(constants.BACKEND_URL + "photo?photo_type=photos/vihar/"  + "&photo_name="  + model.vihar_photo);
		$.from_date.setText("From date :   " + model.from_date);
		$.to_date.setText("To date  :   " + model.to_date);
		$.contact_person1.setText("Contact person 1 :   " + model.contact_person1);
		$.contact_num1.setText("mobile : " + model.contact_num1);
		$.contact_person2.setText("Contact person 2 : " + model.contact_person2);
		$.contact_num1.setText("mobile : " + model.contact_num2);
		$.description.setText("Description : " + model.description);
		//$.facebook.setText("Facebook:" + model.facebook);
		//$.eventvideo.setVideo(model.videolink);
		$.vihar_state.setText("Address : "+ model.vihar_state);
		//$.summary.setText(model.summary);

	}
	function replaceAll(find, replace, str) {
		var re = new RegExp(find, 'g');
		str = str.replace(re, replace);
		return str;
	}

	// execute constructor with optional arguments passed to controller
})(arguments[0] || {});

/////RSVP/////

function rowimage() {
	Alloy.createController('arham_photo', {
		event : event.id,
		image : event.invitation,
		name : event.name
	}).getView().open();
	
}

function submitRsvp() {
	Alloy.createController('registration', {
		event : event.id,
		type : event.event_type
	}).getView().open();
}

/**
 * Event listener set via view to be called on when the user taps the home-icon (Android)
 */
function close() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('calender_vihar_list').getView().open();
}

function mobile1() {
	Titanium.Platform.openURL('tel:' + event.contact_number1);
}

function mobile2() {
	Titanium.Platform.openURL('tel:' + event.contact_number2);
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}
