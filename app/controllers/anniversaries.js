var constants = require('constants');
var member_util = require('member_util');

var anniversary = Alloy.createModel('anniversary');

var sections = [];

var presentAnniversary = [];
var pastAnniversary = [];
var futureAnniversary = [];

anniversary.fetch({
	success : function() {
		var response = anniversary.toJSON();
		presentAnniversary = response.present_aday;
		futureAnniversary = response.future_aday;
		pastAnniversary = response.past_aday;
		

		sections.push(buildSection("Today's", response.present_aday));
		sections.push(buildSection("Upcoming", response.future_aday));
		sections.push(buildSection("Past", response.past_aday));
		$.anniversaryListView.sections = sections;
	},
	error : function() {
		alert("Check internet connection");		  
	}
	
});

function buildSection(section_name, anniversary_collection) {
	var section = Ti.UI.createListSection();
	section.headerTitle = section_name;
	section.template = "rowTemplate";
	section.items = buildDobCollection(anniversary_collection);
	return section;
}

function buildDobCollection(anniversary_collection) {
	var itemCollection = [];
	for (var i in anniversary_collection) {
		var tmp = {
			dob : {
				text : anniversary_collection[i].dom
			},
			mem_name : {
				text : anniversary_collection[i].mem_name
			},
			sp_name : {
				text : anniversary_collection[i].sp_name
			},
			memberImage : {
				image : constants.BACKEND_IMAGE_URL+"members/"+ anniversary_collection[i].photo
			},
			spouseImage : {
				image : constants.BACKEND_IMAGE_URL+"members/"+ escape(anniversary_collection[i].sp_photo)
			},
			template : 'rowTemplate'
		};
		itemCollection.push(tmp);
	}
	return itemCollection;
}

function cleanup() {
	$.destroy();
}

function select(e) {
	var model;
	var selectedMemberType = e.bindId;
	switch(e.sectionIndex) {
	case 0:
		model = presentAnniversary[e.itemIndex];
		break;
	case 1:
		model = futureAnniversary[e.itemIndex];
		break;
	case 2:
		model = pastAnniversary[e.itemIndex];
		break;
	}
	var memberName;
	var memberMobile;
	if (selectedMemberType == 'mem_name') {
		memberName = model.mem_name;
		memberMobile = model.mem_mobile;
	} else {
		memberName = model.sp_name;
		memberMobile = model.sp_mobile;
	}
	member_util.createCallSmsWhatsappDialog(memberName, memberMobile);
}

function close() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}