var constants = require('constants');
var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
Ti.API.info(data);
//Ti.API.info(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[0].image);
Ti.API.info(data.ad_details.length);

if (data.ad_details.length > 0){
var currentAd = 0;
//$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ 0 ].image );
setInterval(function(){ 
	
	
	if( currentAd >= data.ad_details.length-1 ){
		currentAd = 0;
	}else{
		currentAd+=1;
	}
	$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ currentAd ].image );
	 }, 3500);

}
function adclick(e){
				var ad_dialog = Titanium.UI.createOptionDialog({
					options : ['Call','Open in browser','Cancel'],
					cancel : 3,
				});
				ad_dialog.show();
				
				
				ad_dialog.addEventListener('click', function(e) {
					
					switch(e.index) {
					
					case 0:
						Titanium.Platform.openURL('tel:' + data.ad_details[ currentAd ].mobileno);
						break;
					case 1:
						Ti.Platform.openURL( data.ad_details[ currentAd ].linkout);
						break;
					}
				});
			}
		



var members = Alloy.Collections.members;
members.fetch({
	success:function(){},
	error:function(){
		alert('Please Check Internet Connection');
	}
});

$.activityIndicator.show();

function transformMemberInfo(model) {
	$.activityIndicator.hide();
    // Need to convert the model to a JSON object
    var transform = model.toJSON();
    transform.photo = constants.BACKEND_IMAGE_URL+"members/" + escape(transform.photo);
    transform.business = transform.business;    
    return transform;
}

/**
 * event listener set via view for when the user selects a ListView item
 * @param  {Object} e Event
 */
function select(e) {
	$.activityIndicator.show();
	'use strict';
	// lookup the model based on the index.
	var model = members.at(e.itemIndex);
	// create the detail controller with the model and get its view
	Alloy.createController('member_detail', {
		id: model.id
	}).getView().open();
	$.activityIndicator.hide();
}

function close() {
	'use strict';
	// close the window, showing the master window behind it
	$.navWin.close();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	 Alloy.createController('index').getView().open();
}



function cleanup() {
    $.destroy();
}