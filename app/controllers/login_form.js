var constants = require('constants');
// advertisement
var adDetails = Alloy.createModel('advertisement');
var data = '';
var homeAd = [];

adDetails.fetch({
	success : function() {
		var response = adDetails.toJSON();
		adHandler(true, response);
	},
	error : function() {
		if (Ti.App.Properties.getBool('dbIntialized')) {
			adHandler(false, offline.fetchAdvertisement());
		}
	}
});

function adHandler(isOnline, response) {
	Ti.App.Properties.setString("adDetails", isOnline ? JSON.stringify(response) : response);
	var fullAdDetails = Ti.App.Properties.getString('adDetails');
	data = JSON.parse(fullAdDetails);
	if (data.ad_fullpage.length > 0) {
		for (var i = 0; i < data.ad_fullpage.length; i++) {
			if (data.ad_fullpage[i].ad_screen == "Home") {
				homeAd.push("1");
			}
		}
	}
}

var login_util = require('login_util');

function actionLogin(e) {
	if (!$.inputUsername.value || !$.inputPassword.value || !$.inputSurname.value || !$.inputcity.value) {
		var dialog = Ti.UI.createAlertDialog({
			message : L('formMissingFields', 'Please complete all form fields'),
			ok : 'OK',
			title : L('actionRequired', 'Action Required')
		}).show();
	} else {
		Ti.App.Properties.setString("mobile", $.inputPassword.value);
		Ti.App.Properties.setString("firstname", $.inputUsername.value);
		Ti.App.Properties.setString("surnmame", $.inputSurname.value);
		Ti.App.Properties.setString("city", $.inputcity.value);
		gotoHome();
	}
}

function gotoHome() {
	if (homeAd.length > 0) {
		Alloy.createController('fullpageadv').getView().open();
	} else {
		Alloy.createController('index').getView().open();
	}
}


function hideKeyboard(e){
  $.inputPassword.blur();
}
//
// View Language
//
$.loginForm.title = L('login', 'Login');
$.inputUsername.hintText = L('loginname', 'Firstname');
$.inputPassword.hintText = L('Mobile number', 'Mobile number');
$.buttonLogin.title = L('login', 'Login');
$.inputSurname.hintText = L('surname', 'Surname');
$.inputcity.hintText = L('city', 'City');
