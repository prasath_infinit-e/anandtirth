/**
 * self-executing function to organize otherwise inline constructor code
 * @param  {Object} args arguments passed to the controller
 */
var constants = require('constants');

//advertisemnet
var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
Ti.API.info(data);
//Ti.API.info(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[0].image);
Ti.API.info(data.ad_details.length);

if (data.ad_details.length > 0){
var currentAd = 0;
//$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ 0 ].image );
setInterval(function(){ 
	
	
	if( currentAd >= data.ad_details.length-1 ){
		currentAd = 0;
	}else{
		currentAd+=1;
	}
	$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ currentAd ].image );
	 }, 3500);

}
function adclick(e){
	  var ad_dialog = Titanium.UI.createOptionDialog({
					options : ['Call','Website','Cancel'],
					cancel : 3,
				});
				ad_dialog.show();
				
				
				ad_dialog.addEventListener('click', function(e) {
					
					switch(e.index) {
					
					case 0:
						Titanium.Platform.openURL('tel:' + data.ad_details[ currentAd ].mobileno);
						break;
					case 1:
						Ti.Platform.openURL( data.ad_details[ currentAd ].linkout);
						break;
					}
				});
			}

//boddetailview


(function constructor(args) {

	// use strict mode for this function scope
	'use strict';

	// model passed to the controller
	var model = args.model.toJSON();
	if (model) {
		$.bodImage.setImage(constants.BACKEND_IMAGE_URL+"members/"+ model.photo);
		$.name_lbl.setText(model.membername);
		$.post.setText(model.post);
		$.mbl_lbl.setText(model.mobile);
		$.dob.setText(new Date(model.dob).toLocaleDateString('en-in'));
		$.business.setText(model.business);
		$.blood.setText(model.bloodgroup);
		$.email_lbl.setText(model.email);
		$.res_add1.setText(model.res_add1);
		$.res_add2.setText(model.res_add2);
		$.res_add3.setText(model.res_add3);
		$.res_city.setText(model.res_city);
	}

	// execute constructor with optional arguments passed to controller
})(arguments[0] || {});

/**
 * Event listener set via view to be called on when the user taps the home-icon (Android)
 */
	$.mob.addEventListener('click', function(e) {
		var member_util = require('member_util');
		member_util.createCallSmsWhatsappDialog($.name_lbl.text, $.mbl_lbl.text);
	});

		
		$.email_lbl.addEventListener('click', function(e) {
					var emailDialog = Titanium.UI.createEmailDialog();
					if (!emailDialog.isSupported()) {
						Ti.UI.createAlertDialog({
							title : 'Error',
							message : 'Email not available on this device.'
						}).show();
						return;
					}
					emailDialog.setSubject(' Gmail ');
					emailDialog.setToRecipients([$.email_lbl.text]);
					emailDialog.setMessageBody('Hi,\n');
					emailDialog.setHtml(false);
					// emailDialog.setBarColor('#336699');
					emailDialog.open();
					emailDialog.addEventListener('complete', function(e) {
						if (e.result == emailDialog.SENT) {
							var email_alert = Ti.UI.createAlertDialog({
								message : 'Email sent successfully',
								ok : 'OK',
								title : 'Email'
							});
							email_alert.show();
						} else {
							var err_alert = Ti.UI.createAlertDialog({
								message : 'Email is not sent',
								ok : 'OK',
								title : 'Email Error'
							});
							err_alert.show();
						}
					});
				});			
			
function close() {
	'use strict';
	// close the window, showing the master window behind it
	$.navWin.close();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	 Alloy.createController('index').getView().open();
}