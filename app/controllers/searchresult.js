var constants = require('constants');
var args = $.args;
var sections=[];
var response= args.response;
//transformsearchInfo(response.search_results);
Ti.API.info("SEARCH INFO: "+response.search_results[0].id);
sections.push(buildSection(response.search_results));
$.SearchListView.sections = sections;
/*function transformsearchInfo(res) {
    // Need to convert the model to a JSON object
    var transform = res;
    Ti.API.info(transform.id);
    Ti.API.info(transform.name);
    transform.photo = "http://codehouse.in/clubappv3_uploads/members/" + transform.photo;
    transform.dob = "Date of Birth : " + transform.dob;    
    return transform;
    
}*/

function buildSection(search_collection) {
	var section = Ti.UI.createListSection();
	//section.headerTitle = section_name;
	section.template = "rowTemplate";
    section.items = buildDobCollection(search_collection);
	return section;	
}


function buildDobCollection(search_collection) {
	var itemCollection = [];
	for (var i in search_collection) {
		var tmp = {
	        dob : {
	            text : new Date(search_collection[i].dob).toLocaleDateString('en-in')
	        },
	        name : {
	            text : search_collection[i].name
	        },
	        rowImage : {
	        	image : constants.BACKEND_IMAGE_URL+"members/" + escape(search_collection[i].photo)
	        },
	        template : 'rowTemplate'
		};
		itemCollection.push(tmp);
	}
	return itemCollection;
}

function select(e) {
    var model = response.search_results[e.itemIndex];
	// create the detail controller with the model and get its view
	Alloy.createController('member_detail', {
		member_detail: model
	}).getView().open();
}

function cleanup() {
    $.destroy();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	 Alloy.createController('index').getView().open();
}


function close() {
	'use strict';
	// close the window, showing the master window behind it
	$.navWin.close();
}
