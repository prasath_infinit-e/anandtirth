var constants = require('constants');
var adDetails = Alloy.createModel('advertisement');
var offline = require('refresh');
var data = '';
var homeAd = [];
Titanium.UI.iPhone.appBadge = 0;

adDetails.fetch({
	success : function() {
		var response = adDetails.toJSON();
		adHandler(true, response);
	},
	error : function() {
		if (Ti.App.Properties.getBool('dbIntialized')) {
			adHandler(false, offline.fetchAdvertisement());
		}
	}
});

function adHandler(isOnline, response) {
	Ti.App.Properties.setString("adDetails", isOnline ? JSON.stringify(response) : response);
	var fullAdDetails = Ti.App.Properties.getString('adDetails');
	data = JSON.parse(fullAdDetails);
	if (data.ad_fullpage.length > 0) {
		for (var i = 0; i < data.ad_fullpage.length; i++) {
			if (data.ad_fullpage[i].ad_screen == "Home") {
				homeAd.push("1");
			}
		}
	}
}


function adclick(e) {
	var ad_dialog = Titanium.UI.createOptionDialog({
		options : ['Call', 'Website', 'Cancel'],
		cancel : 3,
	});
	ad_dialog.show();

	ad_dialog.addEventListener('click', function(e) {

		switch(e.index) {

		case 0:
			Titanium.Platform.openURL('tel:' + data.ad_details[currentAd].mobileno);
			break;
		case 1:
			Ti.Platform.openURL(data.ad_details[currentAd].linkout);
			break;
		}
	});
}


//mainview

var view = Ti.UI.createView({
	height : '100%',
	top : 0,
	width : '100%',
	right : 0,
	backgroundImage : 'bg.png',

});
$.Fullview.add(view);



//Ti.API.info("Celebrations" + notifyCelebration);



//icons
var person = Ti.UI.createImageView({
	image : '/aboutus.png',
	width : "55%",
	height : "32%",
	top : "4%",
		left : "24%",
	//backgroundColor:'white'
});
view.add(person);

var arham = Ti.UI.createImageView({
	image : '/arham.jpg',
	width : "50%",
	height : "15%",
	top : "41%",
		left : "1%",
	//  backgroundColor:'transparent'
});
view.add(arham);
arham.addEventListener('click', function() {
	Alloy.createController('arham').getView().open();
});

var arhat = Ti.UI.createImageView({
	image : '/arhat.png',
	width : "47%",
	height : "15%",
	top : "41%",
		left : "52%",
	//  backgroundColor:'transparent'
});
view.add(arhat);
arhat.addEventListener('click', function() {
	Alloy.createController('system').getView().open();
});


var social = Ti.UI.createImageView({
	image : '/s.jpg',
	width : "50%",
	height : "15%",
	top : "57%",
		left : "1%",
	//  backgroundColor:'transparent'
});
view.add(social);
social.addEventListener('click', function() {
	Alloy.createController('socialmedia').getView().open();
});

var stavan = Ti.UI.createImageView({
	image : '/stavan',
	width : "47%",
	height : "15%",
	top : "57%",
		left : "52%",
	//  backgroundColor:'transparent'
});
view.add(stavan);
stavan.addEventListener('click', function() {
		Alloy.createController('stavan').getView().open();
});

var donation = Ti.UI.createImageView({
	image : '/donation.jpg',
	width : "50%",
	height : "15%",
	top : "73%",
		left : "1%",
	//  backgroundColor:'transparent'
});
view.add(donation);
donation.addEventListener('click', function() {
			Ti.Platform.openURL('http://www.anandtirth.com/donation');
	});

var pravachan = Ti.UI.createImageView({
	image : '/p.jpg',
	width : "47%",
	height : "15%",
	top : "73%",
		left : "52%",
	//  backgroundColor:'transparent'
});
view.add(pravachan);
pravachan.addEventListener('click', function() {
	Alloy.createController('youtube').getView().open();
});

/* var academics = Ti.UI.createImageView({
	image : '/academics.png',
	width : "20%",
	height : "11%",
	top : "42%",
		left : "6%",
	//  backgroundColor:'transparent'
});
view.add(academics);
academics.addEventListener('click', function() {
	Alloy.createController('events').getView().open();
});
var academicslbl = Ti.UI.createLabel({
		text : 'ARHAM',
		top : '54%',
		left : '7%',
		font : {
			fontFamily : 'Helvetica',
			fontWeight : 'bold'
		},
		color : 'white'
	});
	view.add(academicslbl);
var system = Ti.UI.createImageView({
	image : '/system.png',
	width : "20%",
	height : "11%",
	top : "42%",
		left : "39%",
	//  backgroundColor:'transparent'
});
view.add(system);
system.addEventListener('click', function() {
	Alloy.createController('system').getView().open();
});
var systemlbl = Ti.UI.createLabel({
		text : 'ARHAT',
		top : '54%',
		left : '41%',
		font : {
			fontFamily : 'Helvetica',
			fontWeight : 'bold'
		},
		color : 'white'
	});
	view.add(systemlbl);
var socialmedia = Ti.UI.createImageView({
	image : '/socialmedia.png',
	width : "20%",
	height : "11%",
	top : "42%",
		left : "70%",
	//  backgroundColor:'transparent'
});
view.add(socialmedia);
socialmedia.addEventListener('click', function() {
	Alloy.createController('socialmedia').getView().open();
});
var socialmedialbl = Ti.UI.createLabel({
		text : 'SOCIAL MEDIA',
		top : '54%',
		left : '66%',
		font : {
			fontFamily : 'Helvetica',
			fontWeight : 'bold'
		},
		color : 'white'
	});
	view.add(socialmedialbl);
var registration = Ti.UI.createImageView({
	image : '/registration.png',
	width : "20%",
	height : "11%",
	top : "61%",
 left : "6%",
	//  backgroundColor:'transparent'
});
view.add(registration);
registration.addEventListener('click', function() {
		Alloy.createController('registration').getView().open();
});
var registrationlbl = Ti.UI.createLabel({
		text : 'REGISTRATION',
		top : '73%',
		left : '2%',
		font : {
			fontFamily : 'Helvetica',
			fontWeight : 'bold'
		},
		color : 'white'
	});
	view.add(registrationlbl);
var donation = Ti.UI.createImageView({
	image : '/donation.png',
	width : "20%",
	height : "11%",
	top : "61%",
	left : "39%",
	//  backgroundColor:'transparent'
});
view.add(donation);
donation.addEventListener('click', function() {
			Ti.Platform.openURL('http://www.anandtirth.com/donation');
	});
var donationlbl = Ti.UI.createLabel({
		text : 'DAAN',
		top : '73%',
		left : '43%',
		font : {
			fontFamily : 'Helvetica',
			fontWeight : 'bold'
		},
		color : 'white'
	});
	view.add(donationlbl);
var pravachan = Ti.UI.createImageView({
	image : '/pravachan.png',
	width : "20%",
	height : "11%",
	top : "61%",
	left : "70%",
	//  backgroundColor:'transparent'
});
view.add(pravachan);
pravachan.addEventListener('click', function() {
	Alloy.createController('youtube').getView().open();
});
var pravachanlbl = Ti.UI.createLabel({
		text : 'PRAVACHAN',
		top : '73%',
		left : '68%',
		font : {
			fontFamily : 'Helvetica',
			fontWeight : 'bold'
		},
		color : 'white'
	});
	view.add(pravachanlbl); */


//Search

function Searchbutton() {

	var searchModel = Alloy.createModel('searchresult');

	var query = $.searchBar.value;
	var name = "";
	var phone = "";

	if (isNaN(query)) {
		name = query;
	} else {
		phone = query;
	}

	searchModel.fetch({
		urlparams : {
			"name" : name,
			"phone" : phone

		},
		success : function() {
			var response = searchModel.toJSON();
			if (response.search_results.length > 0) {
				Alloy.createController('searchresult', {
					response : response
				}).getView().open();
			} else {
				alert("No Result Found");
			}
			//Ti.API.info("search response" + response.search_results[0].id);
		},
		error : function() {
			Ti.API.info("login error");
		}
	});
}

function notification() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('livefeed').getView().open();
}

