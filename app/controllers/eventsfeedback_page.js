var constants = require('constants');


var loginResponse;
var loginPrefResponse = Ti.App.Properties.getString('mobile');
var loginDetails = JSON.parse(loginPrefResponse);
Ti.API.info("This is the longin Details " + loginDetails);
loginResponse = loginDetails;

//EventDetailview
var args = $.args;
var id = args.id;
var eventname = args.name;
var model = args.model;
var email1 = model.email1;
var email2 = model.email2;
var email3 = model.email3;

var strEventName = replaceAll(" ", "_", eventname);
$.photo.setImage(constants.BACKEND_IMAGE_URL + "events/" + id + "_" + strEventName.toLowerCase() + "/" + model.invitation);

function replaceAll(find, replace, str) {
		var re = new RegExp(find, 'g');
		str = str.replace(re, replace);
		return str;
	}

$.date.setText("Date : " + model.eventdate);
$.name.setText("Name : " + eventname);
$.description.setText(model.summary);


$.feedback.hintText = $.feedback.value;
$.feedback.addEventListener('focus', function(e) {
	if (e.source.value == e.source.hintText) {
		e.source.value = "";
	}
});
$.feedback.addEventListener('blur', function(e) {
	if (e.source.value == "") {
		e.source.value = e.source.hintText;
	}
});

$.btn.addEventListener('click', function(e) {
	if (Ti.Network.online) {
		var xhrpost = Ti.Network.createHTTPClient();
		$.activityIndicator.show();
		xhrpost.onload = function() {
			Ti.UI.createAlertDialog({
				message : 'Thank you for the valuable feedback.Have a nice day..!!!',
				ok : 'OK',
				title : 'Events Feedback'
			}).show();
		};
		xhrpost.onerror = function() {
		};
		var ebody = 'Feedback:' +    $.feedback.value      + '        ' + 'Mobile No:' + loginDetails ;
		//var ebody = encodeURIComponent(ebody);
		var userId = 'Event Name:' + eventname + ' ARHAM (Events Feedback)';
		var posturl = 'http://www.codehouse.in/clubappv3/api/email?to=manoj@infinit-e.in,' + email1 + ',' + email2 + ',' + email3 + '&subject=' + userId + '&message=' + ebody;
		Ti.API.info(posturl);
		xhrpost.open('POST', posturl);
		xhrpost.send();
		$.activityIndicator.hide();
	} else {
		alert('please connect to the internet to submit the feedback');
		$.activityIndicator.hide();
	}
});


/**
 * Event listener set via view to be called on when the user taps the home-icon (Android)
 */
function close() {
	'use strict';
	// close the window, showing the master window behind it
	$.navWin.close();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}
