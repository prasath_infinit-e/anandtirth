var constants = require('constants');

//Advertisement

/*var constants = require('constants');
//advertisemnet
var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
var currentAd = 0;

if (data.ad_details.length > 0){

//$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ 0 ].image );
setInterval(function(){ 
	
	
	if( currentAd >= data.ad_details.length-1 ){
		currentAd = 0;
	}else{
		currentAd+=1;
	}
	$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ currentAd ].image );
	 }, 3500);

}
function adclick(e){
	  var ad_dialog = Titanium.UI.createOptionDialog({
					options : ['Call','Website','Cancel'],
					cancel : 3,
				});
				ad_dialog.show();
				
				
				ad_dialog.addEventListener('click', function(e) {
					
					switch(e.index) {
					
					case 0:
						Titanium.Platform.openURL('tel:' + data.ad_details[ currentAd ].mobileno);
						break;
					case 1:
						Ti.Platform.openURL( data.ad_details[ currentAd ].linkout);
						break;
					}
				});
			}*/






//window
var about = Ti.UI.createLabel({
  text : "Upadhyay Shri Pravin Rishi: Life and Achievements\nAmong the charismatic and influential Jain monks of today, Upadhyay Shri Pravin Rishi is held in high regard and esteem. Upadhyay Shri was born to pious mother Smt. Champabai Desarda and devout father Shri Dagdulalji Desarda on 7th October, 1957 in Ghodegaon (Ahmednagar District, Maharashtra). A standing example of how great men are not formed only through formal schooling, Shri Pravin Rishi studied upto class X and had always been inclined towards deeper religious philosophies. As a result, he would eagerly participate in religious activities. His interest for Dharma grew manifold and changed into detachment from matters of the world. At only 16 years of age, he accepted the vows of a Jain Monk (Diksha) at the lotus feet of Acharya Shri Anandrishiji Maharaj of the Shraman Sangh on 24th March, 1974 at Bhusaval (Khandesh, Maharashtra).\n\nAs he started treading on this noble path with self-control (Sanyam), he set most of his time towards Gurubhakti and study of the Aagams. He spent 18 years under the guidance of Acharya Shri Anand Rishiji and served as the epitome of dedication and devotion to his Guru till his last day. Under the mentorship of his Guru, Pravin Rishi studied the Jain Aagams in great detail. Not only did he study, he also analysed, reiterate and practiced what he learnt. His inquisitive nature shaped him into a multifaceted personality. This is perhaps why his sermons attract listeners from all walks of life. He is known to hit the orthodox and superstitious mind-set of society with his logical observations. A person only has to listen to one of his sermons to become an ardent follower.\n\nIn order to solidify the clay of his knowledge in the kiln of meditation, Shri Pravin Rishi practiced Divine Sadhana in the solitude of the forests near Indore (Madhya Pradesh) for 13 months around the year 2003. Taking note of his vast knowledge and Sadhana, Acharya Shri Dr. Shivmuniji (4th Patdhar of the Shraman Sangh) bestowed him with the title of Upadhyay in 2004. He has been practicing Ekanthar Tap (fasting on alternate days) for the past six years. With the intent of giving more and most to society, Upadhyay Shri works delicately on his sermons, programmes and Shivirs for 16-17 hours a day.\n\nUntil now, he has spent 23 Chaturmas on his own, influencing innumerable lives and transforming societies in the process. Through his wise words, he has motivated people in Maharashtra, Madhya Pradesh, Rajasthan, Delhi, Uttar Pradesh, Haryana, Punjab, Andhra Pradesh, Tamil Nadu and Karnataka. He traverses over 2500 km a year on foot in order to propagate the words of Lord Mahavir. \nThrough his unique narrative style, Upadhyay Shri Pravin Rishi is perhaps the only monk to bring alive the life-story and messages of Lord Mahavir in the form of the Mahavir Gatha. He guides us along the journey of the Lord\u2019s life with such deep intensity that listeners feel as if transported to the same timeline. Along the same lines, the Uttaradhyayan Sutra- Mahavir\u2019s last sermons- are narrated by him with much enchantment making the experience one-of-its-kind. Through such endeavours, he has ignited a feeling of divine faith in countless hearts.\n\nThough based on scripture and tradition, all his programs cater to the needs of and seek to positively transform the modern society. To this end, he has struck a balance between tradition and modernity, between religion and science, between theory and practice. His initiatives- Navkar Kalash and Gautam Nidhi- are continuously changing the face of society. His sermons on Karma Siddhanth (Secrets of Karma) highlight the depth of his knowledge. Similarly, he has presented a new approach to the traditional Samayik, Pratikraman, Tapasya and Dhyan.\n\nUpadhyay Shri\u2019s roster of endeavours to inspire spiritual change are increasing by the year. Wherever he notices darkness, superstition, confusion or deficiency in the familial, societal or dharmic spheres- he brings brightness through the flashlight of a Shivir or other programme. Through his Shivirs, he has touched the lives of people from all strata of society regardless of age, interest or gender. These include Discover Yourself, Happy Married Life, Happy Home , Arham Garbh Sadhana, Arham Dharna, Parenting, Rasoi Ghar ko Mandir Banayein, Joy on Sale, Purushakar and Ashtamangal Dhyan Sadhana among others. Each and every program provides for holistic growth of the learner.\n\nAmong these, Purushakar Parakram Dhyan Sadhana is a simple, easy to follow, unique and universal meditation technique revived from the Aagams by Upadhyay Shri Pravin Rishi. Practicing this Sadhana with colour therapy, mantra therapy and form therapy assists in opening up one\u2019s inner vision thereby facilitating internal transformation and self-purification thereby leading one on the path of divine self-realisation.\n\nFor more details, visit www.anandtirth.com",
textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
color : "white"
	
});
$.scrollview.add(about);





$.aboutWindow.open();


function close() {
	'use strict';
	Ti.API.info('close');
	// close the window, showing the master window behind it
	$.aboutWindow.close();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	 Alloy.createController('index').getView().open();
}