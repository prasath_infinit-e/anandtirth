var constants = require('constants');

//advertisement
var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
Ti.API.info(data);
//Ti.API.info(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[0].image);
Ti.API.info(data.ad_details.length);

if (data.ad_details.length > 0){
var currentAd = 0;
//$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ 0 ].image );
setInterval(function(){ 
	
	
	if( currentAd >= data.ad_details.length-1 ){
		currentAd = 0;
	}else{
		currentAd+=1;
	}
	$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ currentAd ].image );
	 }, 3500);

}
function adclick(e){
				var ad_dialog = Titanium.UI.createOptionDialog({
					options : ['Call','Open in browser','Cancel'],
					cancel : 3,
				});
				ad_dialog.show();
				
				
				ad_dialog.addEventListener('click', function(e) {
					
					switch(e.index) {
					
					case 0:
						Titanium.Platform.openURL('tel:' + data.ad_details[ currentAd ].mobileno);
						break;
					case 1:
						Ti.Platform.openURL( data.ad_details[ currentAd ].linkout);
						break;
					}
				});
			}


//photodisplay

var args=$.args;
var images=args.pictureid;
var pictureposition=args.pictureposition;

Ti.API.info('3');
//Ti.API.info(images);
//Ti.API.info(pictureposition);

	
var photosView = Ti.UI.createScrollableView({
		width : 320,
		height : 480,
		top : 0,
		bottom : "15%",
		bottom :"20%",
		//showPagingControl : true,
		pagingControlColor : '#fff',
		maxZoomScale : 2.0,
		currentPage : pictureposition
	});
     for (var i = 0; i < images.length; i++) {
		var view = Ti.UI.createScrollView({
			showVerticalScrollIndicator : "true",
			showHorizontalScrollIndicator : "false",
			width : "99%",
			height : "80%",
			backgroundColor : 'white',
			maxZoomScale: 10,
           contentWidth: 'auto',
           contentHeight: 'auto',
			top : "2%",
			layout : 'vertical'
		});
		var img = Ti.UI.createImageView({
			image : images[i],
			width : "90%",
			height : Ti.UI.SIZE,
			top :"0",
			index : i,
		});
		img.addEventListener('click', function(e) {
			var scaleWidth = view.size.width / img.size.width;
    var scaleHeight = view.size.height / img.size.height;
    // Set the initial ZoomScale
    // And the MinZoomScale
    view.zoomScale = view.minZoomScale = Math.min(scaleWidth, scaleHeight);
});
		var save = Ti.UI.createImageView({
			image : "/save.png",
			top : 10,
			left : "90%",
			borderRadius : 5,
			width : '30dp',
			height : '30dp'
		});
		view.add(save);
		view.add(img);
		save.img = img;
		photosView.addView(view);
		$.viewphoto.add(photosView);
		save.addEventListener('click', function(e) {
			$.activityIndicator.show();
			var blobObj = e.source.img.toBlob();
			Ti.Media.saveToPhotoGallery(blobObj, {
				success : function(e) {
					alert('Saved image to gallery');
					$.activityIndicator.hide();
				},
				error : function(e) {
					alert("Error trying to save the image.");
					$.activityIndicator.hide();
				}
			});
		});
		}
		
$.photoWindow.open();

function close(){
    $.photoWindow.close();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	 Alloy.createController('index').getView().open();
}

function cleanup() {
    $.destroy();
}