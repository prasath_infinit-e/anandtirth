var constants = require('constants');
var offline = require('refresh');
var isOffline = false;
/*var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
Ti.API.info(data);
Ti.API.info(data.ad_details.length);

if (data.ad_details.length > 0){
var currentAd = 0;
setInterval(function(){ 
	
	
	if( currentAd >= data.ad_details.length-1 ){
		currentAd = 0;
	}else{
		currentAd+=1;
	}
	$.adImgView.setImage(Ti.App.Properties.getBool('dbIntialized') ? Ti.Filesystem.applicationDataDirectory + "/" + data.ad_details[currentAd].image : constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[currentAd].image);
	 }, 3500);

}
function adclick(e){
	  var ad_dialog = Titanium.UI.createOptionDialog({
					options : ['Call','Website','Cancel'],
					cancel : 3,
				});
				ad_dialog.show();
				
				
				ad_dialog.addEventListener('click', function(e) {
					
					switch(e.index) {
					
					case 0:
						Titanium.Platform.openURL('tel:' + data.ad_details[ currentAd ].mobileno);
						break;
					case 1:
						Ti.Platform.openURL( data.ad_details[ currentAd ].linkout);
						break;
					}
				});
			}*/





//View starts
var view = Ti.UI.createView({
	height : '100%',
	top : 0,
	width : '100%',
	right : 0,
	backgroundImage : 'bg.png',

});
$.weblinkview.add(view);

var facebook_social = Ti.UI.createImageView({
	image : '/social/fb.png',
	width : "50%",
	height : "20%",
	top : "3%",
		left : "1%",
	//  backgroundColor:'transparent'
});
view.add(facebook_social);
facebook_social.addEventListener('click', function() {
	Alloy.createController('facebook').getView().open();
});
	
var twitter_social = Ti.UI.createImageView({
	image : '/social/twitter.png',
	width : "47%",
	height : "20%",
	top : "3%",
		left : "52%",
	//  backgroundColor:'transparent'
});
view.add(twitter_social);
twitter_social.addEventListener('click', function() {
	Alloy.createController('twitter').getView().open();
});	
var linked_social = Ti.UI.createImageView({
	image : '/social/linkedin.png',
	width : "50%",
	height : "20%",
	top : "24%",
		left : "1%",
	//  backgroundColor:'transparent'
});
view.add(linked_social);
linked_social.addEventListener('click', function() {
	Alloy.createController('linked_in').getView().open();
});
	
var pinterest_social = Ti.UI.createImageView({
	image : '/social/pinterest.png',
	width : "47%",
	height : "20%",
	top : "24%",
		left : "52%",
	//  backgroundColor:'transparent'
});
view.add(pinterest_social);
pinterest_social.addEventListener('click', function() {
	Alloy.createController('pinterest').getView().open();
});

var googleplus_social = Ti.UI.createImageView({
	image : '/social/g+.png',
	width : "50%",
	height : "20%",
	top : "45%",
		left : "1%",
	//  backgroundColor:'transparent'
});
view.add(googleplus_social);
googleplus_social.addEventListener('click', function() {
	Alloy.createController('googleplus').getView().open();
});


var youtube_social = Ti.UI.createImageView({
	image : '/social/youtube.png',
	width : "47%",
	height : "20%",
	top : "45%",
		left : "52%",
	//  backgroundColor:'transparent'
});
view.add(youtube_social);
youtube_social.addEventListener('click', function() {
	Alloy.createController('youtube').getView().open();
});

	
var instagram_social = Ti.UI.createImageView({
	image : '/social/insta.png',
	width : "50%",
	height : "20%",
	top : "66%",
		left : "1%",
	//  backgroundColor:'transparent'
});
view.add(instagram_social);
instagram_social.addEventListener('click', function() {
	Alloy.createController('instagram').getView().open();
});

var flickr_social = Ti.UI.createImageView({
	image : '/social/flickr.png',
	width : "47%",
	height : "20%",
	top : "66%",
		left : "52%",
	//  backgroundColor:'transparent'
});
view.add(flickr_social);
flickr_social.addEventListener('click', function() {
	Alloy.createController('flickr').getView().open();
});

function close() {
	'use strict';
	// close the window, showing the master window behind it
	$.navWin.close();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function cleanup() {
	$.destroy();
}