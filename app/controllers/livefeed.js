var constants = require('constants');
/*var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
Ti.API.info(data);
//Ti.API.info(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[0].image);
Ti.API.info(data.ad_details.length);

if (data.ad_details.length > 0){
var currentAd = 0;
//$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ 0 ].image );
setInterval(function(){

if( currentAd >= data.ad_details.length-1 ){
currentAd = 0;
}else{
currentAd+=1;
}
$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ currentAd ].image );
}, 3500);

}
function adclick(e){
var ad_dialog = Titanium.UI.createOptionDialog({
options : ['Call','Website','Cancel'],
cancel : 3,
});
ad_dialog.show();

ad_dialog.addEventListener('click', function(e) {

switch(e.index) {

case 0:
Titanium.Platform.openURL('tel:' + data.ad_details[ currentAd ].mobileno);
break;
case 1:
Ti.Platform.openURL( data.ad_details[ currentAd ].linkout);
break;
}
});
}*/

//Eventview

var collection = Alloy.Collections.livefeed;
collection.fetch({
	success : function() {
		$.activityIndicator.show();
		if (collection.toJSON().length > 0) {
			$.activityIndicator.hide();
		} else {
			
			alert("There is no News feed available");
			$.activityIndicator.hide();
		}
	},
	error : function() {
		alert('Please Check Internet Connection');
	}
});

function transformlivefeedInfo(model) {
	$.activityIndicator.show();
				// Need to convert the model to a JSON object
				var transform = model.toJSON();

				transform.live_photo = constants.BACKEND_IMAGE_URL + "photos/" + transform.live_photo;
				transform.date = "Date : " + transform.date;
				return transform;
				$.activityIndicator.hide();
			}


/**
 * event listener set via view for when the user selects a ListView item
 * @param  {Object} e Event
 */
function select(e) {
	$.activityIndicator.show();
	'use strict';
	// lookup the model based on the index.
	var model = collection.at(e.itemIndex);
	// create the detail controller with the model and get its view
	Alloy.createController('livefeed_detail', {
		model : model
	}).getView().open();
}

function close() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function cleanup() {
	$.destroy();
}