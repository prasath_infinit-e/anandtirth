var constants = require('constants');
//advertisemnet
/*var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
Ti.API.info(data);
//Ti.API.info(constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[0].image);
Ti.API.info(data.ad_details.length);

if (data.ad_details.length > 0){
	var currentAd = 0;
	//$.adImgView.setImage(constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[0].image);
	setInterval(function() {

		if (currentAd >= data.ad_details.length - 1) {
			currentAd = 0;
		} else {
			currentAd += 1;
		}
		$.adImgView.setImage(constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[currentAd].image);
	}, 3500);

}
function adclick(e) {
	var ad_dialog = Titanium.UI.createOptionDialog({
		options : ['Call', 'Website', 'Cancel'],
		cancel : 3,
	});
	ad_dialog.show();

	ad_dialog.addEventListener('click', function(e) {

		switch(e.index) {

		case 0:
			Titanium.Platform.openURL('tel:' + data.ad_details[currentAd].mobileno);
			break;
		case 1:
			Ti.Platform.openURL(data.ad_details[currentAd].linkout);
			break;
		}
	});
}*/

var collection = Alloy.Collections.events_feedback;
collection.fetch({
	success : function() {
		$.activityIndicator.show();
		var events_feedback = collection.toJSON();
		Ti.API.info("EVENTSLENGTH " + events_feedback.length);
		if (events_feedback.length > 0) {
			var sections = [];
			sections.push(buildSection(events_feedback));
			$.eventListView.sections = sections;
			//transformEventInfo(collection);
		} else {
			$.activityIndicator.hide();
			alert("There is no events available");
		}
	},
	error : function() {
		alert('Please Check Internet Connection');
	}
});
$.activityIndicator.show();

function buildSection(event_collection) {
	$.activityIndicator.hide();
	var section = Ti.UI.createListSection();
	section.template = "template";
	section.items = buildRowCollection(event_collection);
	return section;
}

function buildRowCollection(event_collection) {

	var itemCollection = [];

	for (var i in event_collection) {

		var tmp = {

			title : {

				text : event_collection[i].name

			},
			subtitle : {
				text : event_collection[i].eventdate
			},
			image : {
				image : constants.BACKEND_IMAGE_URL + "events/" + event_collection[i].id + "_" + replaceAll(" ", "_", event_collection[i].name).toLowerCase() + "/" + event_collection[i].invitation
			},
			template : 'template'
		};

		itemCollection.push(tmp);

	}

	return itemCollection;

}

function transformEventInfo(model) {
	$.activityIndicator.hide();
	// Need to convert the model to a JSON object
	var transform = model.toJSON();
	Ti.API.info("EVENTSLENGTH " + transform.length);
	Ti.API.info("EVENTSLENGTH " + transform[0].name);
	for (var i = 0; i < transform.length; i++) {
		var strEventName = replaceAll(" ", "_", transform[i].name);
		Ti.API.info("event Name: " + strEventName);
		transform[i].invitation = constants.BACKEND_IMAGE_URL + "events/" + transform[i].id + "_" + strEventName.toLowerCase() + "/" + transform[i].invitation;
		Ti.API.info(transform[i].invitation);
		transform[i].eventdate = transform[i].eventdate;
		return transform[i];
	}
}

function replaceAll(find, replace, str) {
	var re = new RegExp(find, 'g');
	str = str.replace(re, replace);
	return str;
}

function select(e) {
	$.activityIndicator.show();
	'use strict';
	// lookup the model based on the index.
	var model = collection.toJSON();
	Ti.API.info("eventId" + model[e.itemIndex].id);
	// create the detail controller with the model and get its view
	Alloy.createController('eventsfeedback_page', {
		model : model[e.itemIndex],
		id : model[e.itemIndex].id,
		name : model[e.itemIndex].name
	}).getView().open();

	$.activityIndicator.hide();
}

function close() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function cleanup() {
	$.destroy();
}