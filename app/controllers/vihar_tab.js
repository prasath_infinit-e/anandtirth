/**
 * self-executing function to organize otherwise inline constructor code
 * @param  {Object} args arguments passed to the controller
 */
(function constructor(args) {

	// use strict mode for this function scope
	'use strict';
 	Alloy.Globals.tabGroup = $.tabGroup;	
 	
	// execute constructor with optional arguments passed to controller
})(arguments[0] || {});

/**
 * Event listener set via view to be called on when the user taps the home-icon (Android)
 */
function close() {
	'use strict';
	// close the window, showing the master window behind it
	$.tabGroup.close();
}