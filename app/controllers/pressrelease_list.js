var constants = require('constants');
//advertisemnet
/*var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
Ti.API.info(data);
Ti.API.info(data.ad_details.length);

if (data.ad_details.length > 0) {
	var currentAd = 0;
	setInterval(function() {

		if (currentAd >= data.ad_details.length - 1) {
			currentAd = 0;
		} else {
			currentAd += 1;
		}
		$.adImgView.setImage(Ti.App.Properties.getBool('dbIntialized') ? Ti.Filesystem.applicationDataDirectory + "/" + data.ad_details[currentAd].image : constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[currentAd].image);
	}, 3500);

}
function adclick(e) {
	var ad_dialog = Titanium.UI.createOptionDialog({
		options : ['Call', 'Website', 'Cancel'],
		cancel : 3,
	});
	ad_dialog.show();

	ad_dialog.addEventListener('click', function(e) {

		switch(e.index) {

		case 0:
			Titanium.Platform.openURL('tel:' + data.ad_details[currentAd].mobileno);
			break;
		case 1:
			Ti.Platform.openURL(data.ad_details[currentAd].linkout);
			break;
		}
	});
}*/

//Eventview
var press = '';
var collection = Alloy.Collections.pressrelease_list;
collection.fetch({
	success : function() {
		$.activityIndicator.show();
		press = collection.toJSON();
		Ti.API.info("GALLERYLENGTH " + press.length);
		if (press.length > 0) {
			var sections = [];
			sections.push(buildSection(press));
			$.projectListView.sections = sections;
			//transformEventInfo(collection);
		} else {
			$.activityIndicator.hide();
			alert("There is no press release available");
		}
	},
	error : function() {
		alert('Please Check Internet Connection');
	}
});
$.activityIndicator.show();

function buildSection(press_collection) {
	$.activityIndicator.hide();
	var section = Ti.UI.createListSection();
	section.template = "template";
	section.items = buildRowCollection(press_collection);
	return section;
}

function buildRowCollection(press_collection) {

	var itemCollection = [];

	for (var i in press_collection) {

		var tmp = {

			title : {

				text : press_collection[i].pressreleasename

			},
			subtitle : {
				text : press_collection[i].date
			},
			image : {
				borderColor : '#808080',
				image : constants.BACKEND_URL + "photo?photo_type=photos/press/" + press_collection[i].id + "&photo_name=" + press_collection[i].images[0]
			},
			properties : {
				height : 75
			},
			template : 'template'
		};

		itemCollection.push(tmp);

	}

	return itemCollection;

}

/* function transformEventInfo(model) {
 $.activityIndicator.hide();
 // Need to convert the model to a JSON object
 var transform = model.toJSON();
 Ti.API.info("EVENTSLENGTH " + transform.length);
 Ti.API.info("EVENTSLENGTH " + transform[0].name);
 for (var i = 0; i < transform.length; i++) {
 var strEventName = replaceAll(" ", "_", transform[i].album_name);
 Ti.API.info("event Name: " + strEventName);
 transform[i].photo = constants.BACKEND_IMAGE_URL + "events/" + transform[i].id + "_" + strEventName.toLowerCase() + "/" + transform[i].photo;
 Ti.API.info(transform[i].invitation);
 return transform[i];
 }
 }*/

function transformEventInfo(model) {
	$.activityIndicator.hide();
	// Need to convert the model to a JSON object
	var transform = model.toJSON();
	Ti.API.info("EVENTSLENGTH " + transform.length);
	Ti.API.info("EVENTSLENGTH " + transform.name);
	var pressName = replaceAll(" ", "_", transform.album_name);
	var pressId = transform.id;
	Ti.API.info("event Name: " + presstName);
	transform.photo = constants.BACKEND_IMAGE_URL + "albums/photo?photo_type=photos/press/" + press_collection[i].id + "&photo_name=" + press_collection[i].images[0];
	Ti.API.info(transform.photo);
	return transform;

}

function replaceAll(find, replace, str) {
	var re = new RegExp(find, 'g');
	str = str.replace(re, replace);
	return str;
}

/**
 * event listener set via view for when the user selects a ListView item
 * @param  {Object} e Event
 */
function select(e) {
	$.activityIndicator.show();
	'use strict';
	// lookup the model based on the index.
	var model = collection.at(e.itemIndex);
	// create the detail controller with the model and get its view
	Alloy.createController('pressrelease_detail', {
		model : model,
	}).getView().open();
	$.activityIndicator.hide();
}

function close() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function cleanup() {
	$.destroy();
}