
var constants = require('constants');

var args = $.args;
var id = args.event;
var strEventName = replaceAll(" ", "_", args.name);
var image = args.image;
Ti.API.info("url : " + constants.BACKEND_URL + "photo?photo_type=events/" + id + "_" + strEventName.toLowerCase() + "&photo_name="  + image );

function replaceAll(find, replace, str) {
		var re = new RegExp(find, 'g');
		str = str.replace(re, replace);
		return str;
	}

	var photosView = Ti.UI.createScrollableView({
		width : 320,
		height : 480,
		top : Ti.Platform.displayCaps.platformHeight * 0.08,
		//showPagingControl : true,
		pagingControlColor : '#fff',
		maxZoomScale : 2.0
	});
		var view = Ti.UI.createView({
			backgroundColor : 'white',
			layout : 'vertical'
		});
		var btn = Ti.UI.createButton({
			title : 'Save',
			backgroundColor : '#00838F',
			color : 'white',
			top : 10,
			id : id,
			borderRadius : 10,
			width : '32%',
			height : '10%'
		});
		var img = Ti.UI.createImageView({
			image : constants.BACKEND_URL + "photo?photo_type=events/" + id + "_" + strEventName.toLowerCase() + "&photo_name="  + image ,
			width : "80%",
			height : "70%",
			top : Ti.Platform.displayCaps.platformHeight * 0.02,
			//canScale : true
		});
		view.add(img);
		view.add(btn);
		btn.img = img;
		photosView.addView(view);
		btn.addEventListener('click', function(e) {
			var blobObj = e.source.img.toBlob();
			Ti.Media.saveToPhotoGallery(blobObj, {
				success : function(e) {
					alert('Saved image to gallery');
				},
				error : function(e) {
					alert("Error trying to save the image.");
				}
			});

		});
	$.win.add(photosView);

function close() {
	'use strict';
	// close the window, showing the master window behind it
	$.ArhamPhoto.close();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function cleanup() {
	$.destroy();
}