//database
function createTableAndInsertData(json) {
	var db = Titanium.Database.open('mydb');
	//var json = JSON.parse(this.responseText);
	//creating advertisement table
	Ti.API.info("Inserting data in datastore");

	if (json.advertisementTable == null) {
		//DO NOTHING
	} else {
		//db.execute('drop table if exists advertisement');
		db.execute('CREATE TABLE if not exists advertisement (id INTEGER, name text, ad_screen text, image text, linkout text, mobileno text);');
		db.execute('delete from advertisement');
		for ( i = 0; i < json.advertisementTable.length; i++) {
			var data_advertisement = json.advertisementTable[i];
			var id = data_advertisement.id;
			var name = data_advertisement.name;
			var ad_screen = data_advertisement.ad_screen;
			var image = data_advertisement.image;
			var linkout = data_advertisement.linkout;
			var mobileno = data_advertisement.mobileno;
			db.execute('insert into advertisement (id, name, ad_screen, image, linkout, mobileno) values (?,?,?,?,?,?)', id, name, ad_screen, image, linkout, mobileno);
			storeadsImages(image);
		}
	}
	//creating events table
	if (json.eventsTable == null) {
		//DO NOTHING
	} else {
		//db.execute('drop table if exists advertisement');
		db.execute('CREATE TABLE if not exists events (id int, name int, eventdate text, location text, summary text, invitation text, showrsvp int, showmember int, showspouse int, showchild int, showguest int, videolink text);');
		db.execute('delete from events');
		for ( i = 0; i < json.eventsTable.length; i++) {
			var data_events = json.eventsTable[i];
			var id = data_events.id;
			var name = data_events.name;
			var eventdate = data_events.eventdate;
			var location = data_events.location;
			var summary = data_events.summary;
			var invitation = data_events.invitation;
			var showrsvp = data_events.showrsvp;
			var showmember = data_events.showmember;
			var showspouse = data_events.showspouse;
			var showchild = data_events.showchild;
			var showguest = data_events.showguest;
			var videolink = data_events.videolink;
			db.execute('insert into events (id, name, eventdate, location, summary, invitation, showrsvp, showmember, showspouse, showchild, showguest, videolink) values (?,?,?,?,?,?,?,?,?,?,?,?);', id, name, eventdate, location, summary, invitation, showrsvp, showmember, showspouse, showchild, showguest, videolink);
			//Ti.API.info(eventdate);
		}
	}
	//creating bday table
	if (json.bdayTable == null) {
	} else {
		//db.execute("drop table if exists bdays");
		db.execute("CREATE TABLE if not exists bdays (id INTEGER, sid INTEGER, name text, dob text, timestamp INTEGER, mobile text, photo text);");
		db.execute("delete from bdays");
		for ( i = 0; json.bdayTable.length > i; i++) {
			var data_bday = json.bdayTable[i];
			var id = data_bday.id;
			var sid = data_bday.sid;
			var name = data_bday.name;
			var dob = data_bday.dob;
			var timestamp = data_bday.curr_bday;
			var mobile = data_bday.mobile;
			var photo = data_bday.photo;
			db.execute("insert into bdays (id, sid, name, dob, timestamp, mobile, photo) values (?,?,?,?,?,?,?)", id, sid, name, dob, timestamp, mobile, photo);
			storeImages(photo);
		}
	}
	//creating bod table
	if (json.bodTable == null) {
		//DO NOTHING
	} else {
		db.execute('CREATE TABLE if not exists bod (id INTEGER, membername text, mid INTEGER, post text, mem_type text);');
		db.execute('delete from bod');
		for ( i = 0; i < json.bodTable.length; i++) {
			db.execute('insert into bod (id, membername, mid, post, mem_type) values (?,?,?,?,?);', [json.bodTable[i].id, json.bodTable[i].membername, json.bodTable[i].mid, json.bodTable[i].post, json.bodTable[i].mem_type]);
		}
	}
	//creating members table
	if (json.membersTable == null) {
		//DO NOTHING
	} else {
		//db.execute('drop table if exists members');
		Ti.App.Properties.setInt('memlength', json.membersTable.length);
		db.execute('CREATE TABLE if not exists members (id INTEGER, membername text, dob text, business text, mobile text, email text, facebook text, twitter text, bloodgroup text, bbm_pin text, dom text, res_add1 text, res_add2 text, res_add3 text, res_city text, res_zip INTEGER, res_phone text, off_add1 text, off_add2 text, off_add3 text, off_city text, off_zip INTEGER, off_phone text, photo text, website text, profcategory text);');
		db.execute('delete from members');
		for ( i = 0; i < json.membersTable.length; i++) {
			var data_members = json.membersTable[i];
			var id = data_members.id;
			var membername = data_members.name;
			var dob = data_members.dob;
			var dom = data_members.dom;
			var business = data_members.business;
			var mobile = data_members.mobile;
			var email = data_members.email;
			var facebook = data_members.facebook;
			var twitter = data_members.twitter;
			var bloodgroup = data_members.bloodgroup;
			var bbm_pin = data_members.bbm_pin;
			var res_add1 = data_members.res_add1;
			var res_add2 = data_members.res_add2;
			var res_add3 = data_members.res_add3;
			var res_city = data_members.res_city;
			var res_zip = data_members.res_zip;
			var res_phone = data_members.res_phone;
			var off_add1 = data_members.off_add1;
			var off_add2 = data_members.off_add2;
			var off_add3 = data_members.off_add3;
			var off_city = data_members.off_city;
			var off_zip = data_members.off_zip;
			var off_phone = data_members.off_phone;
			var photo = data_members.photo;
			var website = data_members.website;
			var profcategory = data_members.profcategory;
			db.execute('insert into members (id, membername, dob, business, mobile, email, facebook, twitter, bloodgroup, bbm_pin, dom, res_add1, res_add2, res_add3, res_city, res_zip, res_phone, off_add1, off_add2, off_add3, off_city, off_zip, off_phone, photo, website, profcategory) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);', id, membername, dob, business, mobile, email, facebook, twitter, bloodgroup, bbm_pin, dom, res_add1, res_add2, res_add3, res_city, res_zip, res_phone, off_add1, off_add2, off_add3, off_city, off_zip, off_phone, photo, website, profcategory);
			storeImages(photo);
		}
	}
	//creating spouse table
	if (json.spouseTable == null) {
		//alert('there is no data');
	} else {
		//db.execute('drop table if exists spouse');
		db.execute('create table if not exists spouse(id INTEGER, mid INTEGER, spname text, dob text, business text, mobile text, email text, facebook text, twitter text, bloodgroup text, bbm_pin text, off_add1 text, off_add2 text, off_add3 text, off_city text, off_zip INTEGER, off_phone text, photo text);');
		db.execute('delete from spouse');
		for ( i = 0; i < json.spouseTable.length; i++) {
			var data_spouse = json.spouseTable[i];
			var id = data_spouse.id;
			var mid = data_spouse.mid;
			var spname = data_spouse.name;
			var dob = data_spouse.dob;
			var business = data_spouse.business;
			var mobile = data_spouse.mobile;
			var email = data_spouse.email;
			var facebook = data_spouse.facebook;
			var twitter = data_spouse.twitter;
			var bloodgroup = data_spouse.bloodgroup;
			var bbm_pin = data_spouse.bbm_pin;
			var off_add1 = data_spouse.off_add1;
			var off_add2 = data_spouse.off_add2;
			var off_add3 = data_spouse.off_add3;
			var off_city = data_spouse.off_city;
			var off_zip = data_spouse.off_zip;
			var off_phone = data_spouse.off_phone;
			var photo = data_spouse.photo;
			db.execute('insert into spouse (id, mid, spname, dob, business, mobile, email, facebook, twitter, bloodgroup, bbm_pin, off_add1, off_add2, off_add3, off_city, off_zip, off_phone, photo) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);', id, mid, spname, dob, business, mobile, email, facebook, twitter, bloodgroup, bbm_pin, off_add1, off_add2, off_add3, off_city, off_zip, off_phone, photo);
			storeImages(photo);
		}
	}
	//creating children table
	if (json.childrenTable == null) {
		//DO NOTHING
	} else {
		//db.execute('drop table if exists children');
		db.execute('CREATE TABLE if not exists children (id INTEGER, mid INTEGER, name text, dob text);');
		db.execute('delete from children');
		for ( i = 0; i < json.childrenTable.length; i++) {
			var child = json.childrenTable[i];
			var childid = child.id;
			var childmid = child.mid;
			var childname = child.name;
			var childdob = child.dob;
			db.execute('insert into children (id, mid, name, dob) values (?,?,?,?);', childid, childmid, childname, childdob);
		}
	}
	db.close();
	Ti.API.info("Done - Inserting data in datastore");

	return true;
}

//photos of members to filesystem function
try {
	function storeImages(filename) {
		var xhr = Titanium.Network.createHTTPClient();
		//xhr.setTimeout(30000);
		xhr.onload = function() {
			try {
				var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, filename);
				f.write(this.responseData);
			} catch(IOException) {
				//Ti.API.info(IOException);
			}
		};
		xhr.onerror = function(e) {
			//alert("check the connection");
		};
		xhr.open('GET', Titanium.App.Properties.getString("urllink") + 'photo?photo_type=members&photo_name=' + filename);
		xhr.send();
	}

} catch(e) {
	//Ti.API.info(e);
}

//photos of advertisement  to filesystem function
try {
	function storeadsImages(filename1) {
		var xhr = Titanium.Network.createHTTPClient();
		//xhr.setTimeout(30000);
		xhr.onload = function() {
			try {
				var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, filename1);
				f.write(this.responseData);
			} catch(IOException) {
				//Ti.API.info(IOException);
			}
		};
		xhr.onerror = function(e) {
		};
		xhr.open('GET', Titanium.App.Properties.getString("urllink") + 'photo?photo_type=ads&photo_name=' + filename1);
		xhr.send();
	}

} catch(e) {
	//Ti.API.info(e);
}
function refresh(callback) {
	Ti.API.info('In refresh');
	if (!Ti.Network.online) {
		alert("You need to be connected to the internet to refresh data");
		return;
	}
	Ti.API.info("Making a Network call");

	var xhr = Ti.Network.createHTTPClient({
		//enableKeepAlive:true
	});
	xhr.setTimeout(40000);
	xhr.onload = function() {
		if (xhr.status == 200) {
			Ti.API.info("Recevied data from backend with status code 200");
			var json = JSON.parse(this.responseText);
			if (this.readyState === 4) {
				if (createTableAndInsertData(json)) {
					Ti.API.info("Done inserting data in datastore");
					var today = new Date().getTime();
					today = today / 1000;
					Titanium.App.Properties.setString("lastupdate", today);
					Ti.App.Properties.setBool('dbIntialized', true);
					callback();
					Ti.API.info("Done calling callback");

				}
			}
		}
	};
	xhr.ondatastream = function() {
		Ti.API.info('xhr connected value load:' + xhr.connected);
	};
	xhr.onerror = function(e) {
		alert("Please check with the internet connection and try again later");
		hideActivityIndicator();
	};
	if (!(Titanium.App.Properties.hasProperty("lastupdate"))) {
		Titanium.App.Properties.setString("lastupdate", "1");
	}
	xhr.open("GET", Titanium.App.Properties.getString("urllink") + "websql?time=" + Titanium.App.Properties.getString("lastupdate"));
	xhr.send();
}

exports.createTableAndInsertData = createTableAndInsertData;
exports.refresh = refresh;
