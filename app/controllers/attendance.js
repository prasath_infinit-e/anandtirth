var constants = require('constants');

//advertisemnt
var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
Ti.API.info(data);
//Ti.API.info(constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[0].image);
Ti.API.info(data.ad_details.length);

if (data.ad_details.length > 0) {
	var currentAd = 0;
	//$.adImgView.setImage(constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[0].image);
	setInterval(function() {

		if (currentAd >= data.ad_details.length - 1) {
			currentAd = 0;
		} else {
			currentAd += 1;
		}
		$.adImgView.setImage(constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[currentAd].image);
	}, 3500);

}
function adclick(e) {
	var ad_dialog = Titanium.UI.createOptionDialog({
		options : ['Call', 'Website', 'Cancel'],
		cancel : 3,
	});
	ad_dialog.show();

	ad_dialog.addEventListener('click', function(e) {

		switch(e.index) {

		case 0:
			Titanium.Platform.openURL('tel:' + data.ad_details[currentAd].mobileno);
			break;
		case 1:
			Ti.Platform.openURL(data.ad_details[currentAd].linkout);
			break;
		}
	});
}

//attendance
var constants = require('constants');
var attendanceModel = Alloy.createModel('attendance');
var attendanceResponse;
var sections = [];

$.activityIndicator.show();
var params = {
	mid : Ti.App.Properties.getString(constants.LOGGED_IN_USER_ID, '')

};
Ti.API.info(Ti.App.Properties.getString(constants.LOGGED_IN_USER_ID, ''));
attendanceModel.fetch({
	urlparams : params,
	success : function() {
		attendanceResponse = attendanceModel.toJSON();
		$.totalevents.setText(attendanceResponse.total_events);
		$.eventsattended.setText(attendanceResponse.events_attended);
		sections.push(buildSection("Events List", attendanceResponse.events));
		$.activityIndicator.hide();
		$.AttendanceListView.sections = sections;

		var percentage = attendanceResponse.events_attended == 0 ? 0 : (attendanceResponse.events_attended * 100) / attendanceResponse.total_events;

		$.eventspercentage.setText(percentage + "%");
	},
	error : function() {
		alert('Check internet connection');
	}
});

function buildSection(section_name, attendance_collection) {
	var section = Ti.UI.createListSection();
	section.headerTitle = section_name;
	section.template = "rowTemplate";
	section.items = buildEventCollection(attendance_collection);
	return section;
}

function buildEventCollection(attendance_collection) {
	var itemCollection = [];
	for (var i in attendance_collection) {
		var tmp = {
			name : {
				text : attendance_collection[i].name
			},
			attended : {
				text : attendance_collection[i].attended == 1 ? 'Yes' : 'No'
			},

			eventdate : {
				text : attendance_collection[i].eventdate
			},
			template : 'rowTemplate'
		};
		itemCollection.push(tmp);
	}
	return itemCollection;
}

function close() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}