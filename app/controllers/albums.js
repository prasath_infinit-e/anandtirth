var constants = require('constants');

//advertisement
var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);

if (data.ad_details.length > 0){
var currentAd = 0;
setInterval(function(){ 
	
	
	if( currentAd >= data.ad_details.length-1 ){
		currentAd = 0;
	}else{
		currentAd+=1;
	}
	$.adImgView.setImage(constants.BACKEND_URL+"photo?photo_type=ads&photo_name=" + data.ad_details[ currentAd ].image );
	 }, 3500);

}
function adclick(e){
				var ad_dialog = Titanium.UI.createOptionDialog({
					options : ['Call','Open in browser','Cancel'],
					cancel : 3,
				});
				ad_dialog.show();
				
				
				ad_dialog.addEventListener('click', function(e) {
					
					switch(e.index) {
					
					case 0:
						Titanium.Platform.openURL('tel:' + data.ad_details[ currentAd ].mobileno);
						break;
					case 1:
						Ti.Platform.openURL( data.ad_details[ currentAd ].linkout);
						break;
					}
				});
			}

//albums view
var args=$.args;
var albumId = args.id;
var totalPhotosCount = args.count;

Ti.API.info('2');
Ti.API.info(albumId);
Ti.API.info(totalPhotosCount);
var fb = require('facebook');
$.win.item = albumId;
var photo_array = [];
var picid=[];
var loadedPhotosCount = 0;
var after;

fb.requestWithGraphPath(albumId + '/photos?fields=images', {}, 'GET', function(e) {
		if (e.success) {
			if (e.result) {
				
				Ti.API.info(e.result);
				var pic = JSON.parse(e.result);
				Ti.API.info(pic.data.length);
				loadedPhotosCount = pic.data.length;
				for (var x = 0; x < pic.data.length; x++) {
					//Ti.API.info(JSON.stringify(pic[x]));
					var photo_id = pic.data[x].id;
					var data = Ti.UI.createImageView({
						image : pic.data[x].images[0].source,
						borderColor : 'red',
						width : 83,
						height : 83,
						left : 12,
						top : 8,
						id : x,
						pid : photo_id
					});
					$.view.add(data);
					photo_array.push(data);
					picid.push(pic.data[x].images[0].source);
					
					}
					
					if( totalPhotosCount > 25 ){
					
					after = pic.paging.cursors.after;
					Ti.API.info( after );
					var data = Ti.UI.createImageView({
						image : '/loadmore.png',
						width : 83,
						height : 83,
						left : 12,
						top : 8,
						id : x,
						pid : photo_id
					});
					$.view.add(data);
					
					}
					
					
			}
	 }
	});
		

$.albumWindow.open();
function imageclick(e){
	
	  var position = e.source.id;
	  Ti.API.info('position' +' '+ position );
	  Ti.API.info( e );
	  Ti.API.info( 'loadedPhotosCount' +' '+ loadedPhotosCount );

	Ti.API.info(picid[e.source.id]);
	
	if( position < loadedPhotosCount ){
			
  var photoWindow = Alloy.createController('album_photo',
  {pictureid : picid,
  pictureposition:position}).getView();
    $.albumWindow.openWindow(photoWindow);

		
	}else{
		loadMorePhotos();
	}
	  
}



function loadMorePhotos(){
	Ti.API.info( 'loadMorePhotos' );
	
	$.view.remove( $.view.children[ loadedPhotosCount ] );
	
fb.requestWithGraphPath(albumId + '/photos?fields=images&after='+after, {}, 'GET', function(e) {
		if (e.success) {
			if (e.result) {
				Ti.API.info(e.result);
				var pic = JSON.parse(e.result);
				Ti.API.info(pic.data.length);
				
				
				for (var x = 0; x < pic.data.length; x++) {
					//Ti.API.info(JSON.stringify(pic[x]));
					var photo_id = pic.data[x].id;
					var data = Ti.UI.createImageView({
						image : pic.data[x].images[0].source,
						borderColor : 'red',
						width : 83,
						height : 83,
						left : 12,
						top : 8,
						id : loadedPhotosCount+x,
						pid : photo_id
					});
					$.view.add(data);
					
					photo_array.push(data);
					picid.push(pic.data[x].images[0].source);
					
					}
					
					loadedPhotosCount += pic.data.length;
				Ti.API.info( 'loadedPhotosCount' +' '+ loadedPhotosCount );
					
					if( loadedPhotosCount < totalPhotosCount ){
					
					after = pic.paging.cursors.after;
					Ti.API.info( after );
					var data = Ti.UI.createImageView({
						image : '/loadmore.png',
						width : 83,
						height : 83,
						left : 12,
						top : 8,
						id : loadedPhotosCount,
						pid : photo_id
					});
					$.view.add(data);
					
					}
					
					
			}
	 }
	});

	
}


function close(){
    $.albumWindow.close();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	 Alloy.createController('index').getView().open();
}
