var birthday = Alloy.createModel('birthday');
var constants = require('constants');
var member_util = require('member_util');

var sections = [];
var presentBday = [];
var pastBday = [];
var futureBday = [];

birthday.fetch({
	success : function() {
	   	var response = birthday.toJSON();
	   	presentBday = response.present_bday;
	   	futureBday = response.future_bday;
	   	pastBday = response.past_bday;
	   	sections.push(buildSection("Today", response.present_bday));
		sections.push(buildSection("Upcoming", response.future_bday));
		sections.push(buildSection("Past", response.past_bday));
		$.birthdayListView.sections = sections;		
	},
	error : function() {
		alert("Check internet connection");		  
	}	
});

function buildSection(section_name, bday_collection) {
	var section = Ti.UI.createListSection();
	section.headerTitle = section_name;
	section.template = "rowTemplate";
    section.items = buildDobCollection(bday_collection);
	return section;	
}

function buildDobCollection(bday_collection) {
	var itemCollection = [];
	for (var i in bday_collection) {
		var tmp = {
	        dob : {
	            text : bday_collection[i].dob
	        },
	        name : {
	            text : bday_collection[i].name
	        },
	        rowImage : {
	        	image : constants.BACKEND_IMAGE_URL +  "/members/" + bday_collection[i].photo
	        },
	        template : 'rowTemplate'
		};
		itemCollection.push(tmp);
	}
	return itemCollection;
}

function cleanup() {
    $.destroy();
}

function select(e) {
	var model;
	switch(e.sectionIndex) {
	case 0:
		model = presentBday[e.itemIndex];
		break;
	case 1:
		model = futureBday[e.itemIndex];
		break;
	case 2:
		model = pastBday[e.itemIndex];
		break;
	}
	member_util.createCallSmsWhatsappDialog(model.name, model.mobile);	
}

function close() {
	'use strict';
	// close the window, showing the master window behind it
	 Alloy.createController('index').getView().open();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	 Alloy.createController('index').getView().open();
}