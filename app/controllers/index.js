var constants = require('constants');
var login_util = require('login_util');
var controls = require('control');
var push_notification = require('register_push_notification');
var offline = require('refresh');

/*var adDetails = Alloy.createModel('advertisement');
var data = '';
var currentAd = 0;

adDetails.fetch({
	success : function() {
		var response = adDetails.toJSON();
		adHandler(true, response);
	},
	error : function() {
		if (Ti.App.Properties.getBool('dbIntialized')) {
			adHandler(false, offline.fetchAdvertisement());
		}
	}
});

function adHandler(isOnline, response) {
	Ti.App.Properties.setString("adDetails", isOnline ? JSON.stringify(response) : response);
	var bannerAdDetails = Ti.App.Properties.getString('adDetails');
	data = JSON.parse(bannerAdDetails);
	Ti.API.info("adData" + data);
	if (data.ad_details.length > 0) {
		setInterval(function() {

			if (currentAd >= data.ad_details.length - 1) {
				currentAd = 0;
			} else {
				currentAd += 1;
			}
			$.adImgView.setImage( isOnline ? constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[currentAd].image : Ti.Filesystem.applicationDataDirectory + "/" + data.ad_details[currentAd].image);
		}, 3500);

	}
}

function adclick(e) {
	var ad_dialog = Titanium.UI.createOptionDialog({
		options : ['Call', 'Website', 'Cancel'],
		cancel : 3,
	});
	ad_dialog.show();

	ad_dialog.addEventListener('click', function(e) {

		switch(e.index) {

		case 0:
			Titanium.Platform.openURL('tel:' + data.ad_details[currentAd].mobileno);
			break;
		case 1:
			Ti.Platform.openURL(data.ad_details[currentAd].linkout);
			break;
		}
	});
}*/

function adclick(e) {
	var ad_dialog = Titanium.UI.createOptionDialog({
		options : ['Call', 'Website', 'Cancel'],
		cancel : 3,
	});
	ad_dialog.show();

	ad_dialog.addEventListener('click', function(e) {

		switch(e.index) {

		case 0:
			Titanium.Platform.openURL('tel:' + data.ad_details[currentAd].mobileno);
			break;
		case 1:
			Ti.Platform.openURL(data.ad_details[currentAd].linkout);
			break;
		}
	});
}

// get main and menu view as objects
var menuView = controls.getMenuView();
var mainView = controls.getMainView();

//variable to controler de open/close slide
var activeView = 1;

// add event listener in this context
menuView.menuTable.addEventListener('click', function(e) {
	$.drawermenu.showhidemenu();
	$.drawermenu.menuOpen = false;
	//update menuOpen status to prevent inconsistency.
	removeCurrentTopView(activeView);
	if (e.rowData.id === "row1") {
		activeView = 1;
	}
	if (e.rowData.id === "row2") {
		if (activeView != 2) {
			Alloy.createController('about_pravin').getView().open();
			activeView = 2;
		} else {
			activeView = 2;
		}
	}
	if (e.rowData.id === "row20") {
		if (activeView != 2) {
			Alloy.createController('About_anand').getView().open();
			activeView = 2;
		} else {
			activeView = 2;
		}
	}
	if (e.rowData.id === "row3") {
		if (activeView != 2) {
			Alloy.createController('underconstruction').getView().open();
			activeView = 2;
		} else {
			activeView = 2;
		}
	}
	if (e.rowData.id === "row18") {
		if (activeView != 2) {
			Alloy.createController('gurunitya_niyam_list').getView().open();
			activeView = 2;
		} else {
			activeView = 2;
		}
	}
	if (e.rowData.id === "row19") {
		if (activeView != 2) {
			Alloy.createController('underconstruction').getView().open();
			activeView = 2;
		} else {
			activeView = 2;
		}
	}
	if (e.rowData.id === "row6") {
		if (activeView != 6) {
			Alloy.createController('livefeed').getView().open();
			activeView = 6;
		} else {
			activeView = 6;
		}
	}
	if (e.rowData.id === "row7") {
		if (activeView != 7) {
			Alloy.createController('project').getView().open();
			activeView = 7;
		} else {
			activeView = 7;
		}
	}
	if (e.rowData.id === "row10") {
		if (activeView != 10) {
			Alloy.createController('gallery').getView().open();
			activeView = 10;
		} else {
			activeView = 10;
		}
	}
	if (e.rowData.id === "row21") {
		if (activeView != 8) {
			// Logout the user. And then redirect to login screen.
			login_util.logoutUser();
			Alloy.createController('askguruji').getView().open();
			activeView = 8;
		} else {
			activeView = 8;
		}
	}
	if (e.rowData.id === "row8") {
		if (activeView != 8) {
			// Logout the user. And then redirect to login screen.
			login_util.logoutUser();
			Alloy.createController('login_form').getView().open();
			activeView = 8;
		} else {
			activeView = 8;
		}
	}
	if (e.rowData.id === "row9") {
		if (activeView != 9) {
			// Logout the user. And then redirect to login screen.
			Alloy.createController('updateprofile').getView().open();
			activeView = 9;
		} else {
			activeView = 9;
		}
	}
	if (e.rowData.id === "row11") {
		if (activeView != 11) {
			// Logout the user. And then redirect to login screen.
			Alloy.createController('pressrelease_list').getView().open();
			activeView = 11;
		} else {
			activeView = 11;
		}
	}
	if (e.rowData.id === "row12") {
		if (activeView != 12) {
			// Logout the user. And then redirect to login screen.
			Alloy.createController('ContactView').getView().open();
			activeView = 12;
		} else {
			activeView = 12;
		}
	}

	if (e.rowData.id === "row13") {
		if (activeView != 13) {
			Alloy.createController('events_feedback').getView().open();
			// Logout the user. And then redirect to login screen.
			activeView = 13;
		} else {
			activeView = 13;
		}
	}
	if (e.rowData.id === "row14") {
		if (activeView != 12) {
			// Logout the user. And then redirect to login screen.
			Alloy.createController('registration').getView().open();
			activeView = 12;
		} else {
			activeView = 12;
		}
	}

	if (e.rowData.id === "row15") {
		if (activeView != 9) {
			// Logout the user. And then redirect to login screen.
			Alloy.createController('vihar_tab').getView().open();
			activeView = 9;
		} else {
			activeView = 9;
		}
	}

	// on Android the event is received by the label, so watch out!
	Ti.API.info(e.rowData.id);
});

function offlineHandler() {
	var offline_dialog = Titanium.UI.createAlertDialog({
		buttonNames : ['Yes', 'No'],
		message : 'Would you like to download Offline Data?',
		title : 'Offline'
	});
	offline_dialog.show();

	offline_dialog.addEventListener('click', function(e) {

		switch(e.index) {

		case 0:
			Ti.API.info("yes");
			offline.refresh('home', function() {

				var data_refresh = Ti.UI.createAlertDialog({
					message : 'Data Updated',
					ok : 'OK'
				});
				data_refresh.show();
				Ti.API.info("offline download complete");
				Ti.App.Properties.setBool('dbIntialized', true);
			});

			break;
		case 1:
			Ti.API.info("no");
			Alloy.createController('index').getView().open();
			break;
		}
	});
}

function removeCurrentTopView(currentActiveView) {
}

function initializeMenu(view) {
	//add menu view to ConfigView exposed by widget
	view.menuButton.add(controls.getMenuButton({
		h : '60',
		w : '60'
	}));

	//Minor changes to click event. Update the menuOpen status;
	view.menuButton.addEventListener('click', function() {
		$.drawermenu.showhidemenu();
		$.drawermenu.menuOpen = !$.drawermenu.menuOpen;
	});
	// method is exposed by widget
}

// First check if the user is logged in or not. If it's not, then redirect him to login screen.
if (!login_util.isUserLoggedIn()) {
	// User is not logged in - redirect to login screen
	Alloy.createController('login_form').getView().open();
} else {
	initializeMenu(mainView);
	$.drawermenu.init({
		menuview : menuView.getView(),
		mainview : mainView.getView(),
		duration : 200,
		parent : $.mainWindow
	});
	push_notification.register();
	$.mainWindow.open();
}

