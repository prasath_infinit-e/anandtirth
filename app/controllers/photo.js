var constants = require('constants');
//advertisemnet
/*var bannerAdDetails = Ti.App.Properties.getString('adDetails');
var data = JSON.parse(bannerAdDetails);
Ti.API.info(data);
Ti.API.info(data.ad_details.length);

if (data.ad_details.length > 0) {
	var currentAd = 0;
	setInterval(function() {

		if (currentAd >= data.ad_details.length - 1) {
			currentAd = 0;
		} else {
			currentAd += 1;
		}
		$.adImgView.setImage(Ti.App.Properties.getBool('dbIntialized') ? Ti.Filesystem.applicationDataDirectory + "/" + data.ad_details[currentAd].image : constants.BACKEND_URL + "photo?photo_type=ads&photo_name=" + data.ad_details[currentAd].image);
	}, 3500);

}
function adclick(e) {
	var ad_dialog = Titanium.UI.createOptionDialog({
		options : ['Call', 'Website', 'Cancel'],
		cancel : 3,
	});
	ad_dialog.show();

	ad_dialog.addEventListener('click', function(e) {

		switch(e.index) {

		case 0:
			Titanium.Platform.openURL('tel:' + data.ad_details[currentAd].mobileno);
			break;
		case 1:
			Ti.Platform.openURL(data.ad_details[currentAd].linkout);
			break;
		}
	});
}*/

//Eventview
var args = $.args;
var id = args.id;
var photo_array = args.photos;
var images = args.pictureid;
var pictureposition = args.pictureposition;
Ti.API.info("position : " + pictureposition );
Ti.API.info("url : " + constants.BACKEND_URL + "photo?photo_type=photos/" + id + "&photo_name=" + pictureposition );



var viewArray = [];
	var photosView = Ti.UI.createScrollableView({
		width : 320,
		height : 480,
		top : Ti.Platform.displayCaps.platformHeight * 0.08,
		//showPagingControl : true,
		pagingControlColor : '#fff',
		maxZoomScale : 2.0,
		currentPage : pictureposition
	});
	for (var i = 0; i < images.length; i++) {
		var view = Ti.UI.createView({
			backgroundColor : 'white',
			layout : 'vertical'
		});
		var btn = Ti.UI.createButton({
			title : 'Save',
			backgroundColor : '#00838F',
			color : 'white',
			top : 10,
			id : i,
			borderRadius : 10,
			width : '32%',
			height : '10%'
		});
		var img = Ti.UI.createImageView({
			image : images[i],
			width :300,
			height : 200,
			top : Ti.Platform.displayCaps.platformHeight * 0.02,
			index : i,
			//canScale : true
		});
		view.add(img);
		view.add(btn);
		btn.img = img;
		photosView.addView(view);
		photosView.currentPage = pictureposition;
		btn.addEventListener('click', function(e) {
			var blobObj = e.source.img.toBlob();
			Ti.Media.saveToPhotoGallery(blobObj, {
				success : function(e) {
					alert('Saved image to gallery');
				},
				error : function(e) {
					alert("Error trying to save the image.");
				}
			});

		});
	}
	$.win.add(photosView);

function close() {
	'use strict';
	// close the window, showing the master window behind it
	$.photoWindow.close();
}

function home() {
	'use strict';
	// close the window, showing the master window behind it
	Alloy.createController('index').getView().open();
}

function cleanup() {
	$.destroy();
}