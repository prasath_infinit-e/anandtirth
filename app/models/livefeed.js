var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "live_feed",
        "adapter": {
            "type": "restapi",
            "collection_name": "livefeed",
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};