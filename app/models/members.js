var constants = require('constants');
exports.definition = {
	config : {
		"URL" : constants.BACKEND_URL + "all_members_list",
		"adapter" : {
			"type" : "restapi",
			"collection_name" : "members",
			"idAttribute" : "id"
		},
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {
			urlRoot : constants.BACKEND_URL,
			url : function() {
				if (this.id == null) {
					var url = this.urlRoot + "/all_members_list";
				} else {
					var url = this.urlRoot + "/load_member?id=" + this.id;
				}
				return url;
			},
		});
		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {});
		return Collection;
	}
}; 