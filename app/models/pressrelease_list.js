var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "pressrelease_list",
        "adapter": {
            "type": "restapi",
            "collection_name": "pressrelease_list"
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};