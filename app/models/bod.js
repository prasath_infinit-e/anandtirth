var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "bod_api",
        "adapter": {
            "type": "restapi",
            "collection_name": "bod",
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};