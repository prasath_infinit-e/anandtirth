var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "login",
        "debug": 1,    	
        "adapter": {
            "type": "restapi",
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};