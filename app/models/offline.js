var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "websql?time=1",
        "adapter": {
            "type": "restapi",
            "collection_name": "livefeed",
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};