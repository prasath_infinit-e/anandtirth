var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "aniv",
        "adapter": {
            "type": "restapi",
            "collection_name": "anniversaries",
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};