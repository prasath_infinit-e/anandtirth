var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "gallery_list",
        "adapter": {
            "type": "restapi",
            "collection_name": "gallery"
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};