var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "search",
        "adapter": {
            "type": "restapi",
            "collection_name": "searchresult",
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};