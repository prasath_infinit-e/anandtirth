var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "futurevihar_api",
        "adapter": {
            "type": "restapi",
            "collection_name": "vihar"
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};