exports.definition = {

    config: {

    "URL": "https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails,snippet&maxResults=50&playlistId=PLvedDtsDxQmAbwGDA2Snlq-rG6-rKjoLe&fields=items&key=AIzaSyAjTbEh_8jMQ0ChZdE3THXAo-N8CjijCPc",

        "adapter": {

            "type": "restapi",

            "collection_name": "youtube",

        },

    },

extendModel: function(Model) {      

       _.extend(Model.prototype, {
			url : function() {
				return "https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails,snippet&maxResults=50&playlistId=" + this.id + "&fields=items&key=AIzaSyAjTbEh_8jMQ0ChZdE3THXAo-N8CjijCPc";
			},
		});

		return Model;

	},


    extendCollection: function(Collection) {        

        _.extend(Collection.prototype, {});

        return Collection;

    }

};