var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "pastevents_api",
        "adapter": {
            "type": "restapi",
            "collection_name": "events_feedback"
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};