var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "attendence",
        "adapter": {
            "type": "restapi",
            "collection_name": "attendance",
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};