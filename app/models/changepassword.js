var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "chgpass",
        "adapter": {
            "type": "restapi",
            "collection_name": "changepassword",
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};