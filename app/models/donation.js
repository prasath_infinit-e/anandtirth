var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "anandtrith_donation",
        "adapter": {
            "type": "restapi",
            "collection_name": "donation"
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};