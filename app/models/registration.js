var constants = require('constants');
exports.definition = {
    config: {
    	"URL": constants.BACKEND_URL + "all_events_list",
        "adapter": {
            "type": "restapi",
            "collection_name": "registration"
        },
    },
	extendModel: function(Model) {      
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {        
        _.extend(Collection.prototype, {});
        return Collection;
    }
};