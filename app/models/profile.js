var constants = require('constants');
exports.definition = {
	config : {

		"URL" : constants.BACKEND_URL + "update_profile",
		"debug" : 1,
		"adapter" : {
			"type" : "restapi",
		},
		"headers": { 
            "enctype": "multipart/form-data"
        }
	},
	extendModel : function(Model) {
		_.extend(Model.prototype, {});
		return Model;
	},
	extendCollection : function(Collection) {
		_.extend(Collection.prototype, {});
		return Collection;
	}
}; 